(ns apps.things.db
  (:require [clojure.java.jdbc :as jdbc]
            [example-server.tools :as dbt]
            [clojure.tools.logging :as log])
  ;(:gen-class)
  )


;;--------------------------------------------------------
;; DB sayings app
;;--------------------------------------------------------


;; CREATE TABLE sayings
;; (
;;  id SERIAL PRIMARY KEY,
;;  ???
;; );
;; INSERT INTO sayings (...) VALUES ('', '');

(def sayings-table-name "sayings")

(def create-table-sayings-str
  (jdbc/create-table-ddl (keyword sayings-table-name) ;;:my_users
                         [[:id "SERIAL" :primary "KEY"]
                          [:content "VARCHAR(1024)" :not "NULL"]]))

;; (defn create-table-sayings
;;   ([db-spec] (create-table-sayings db-spec false false))
;;   ([db-spec drop-if-exists only-if-non-existant]
;;    (if drop-if-exists
;;      (let [sql-exprs [(str  "drop table if exists " sayings-table-name)
;;                       create-table-sayings-str]]
;;        (jdbc/db-do-commands db-spec true sql-exprs))
;;      (let [sql-exprs [create-table-sayings-str]]
;;        (if only-if-non-existant
;;          (if-not (table-exists? db-spec sayings-table-name)
;;            (jdbc/db-do-commands db-spec true sql-exprs)
;;            (log/debug "Table " sayings-table-name " exists already."))
;;          (jdbc/db-do-commands db-spec true sql-exprs)))
;;      )))

(defn create-table-sayings-if-non-existant [db-spec]
  #_((dbt/get-fn-create-table-if-non-existant sayings-table-name
                                              create-table-sayings-str) db-spec)
  (dbt/create-table :db-spec db-spec
                    :table-name sayings-table-name
                    :ddl create-table-sayings-str
                    :drop-if-exists false))

;; (defn create-table-sayings-if-non-existant [db-spec]
;;   (create-table-sayings db-spec false true))



;;;;;;; Getters and Setters


(defn get-sayings-inner [db do-smth]
  (jdbc/with-db-connection [conn db]
    (for [s (jdbc/query conn
                      ["select content from sayings order by id"])]
      (do-smth s)))
   ;; Above is equal to:
   ;; (with-open [conn (jdbc/get-connection db-spec)]
   ;;   (for [s (jdbc/query (env :db-spec)
   ;;                     ["select content from sayings"])]
   ;;     (do-smth s)))
   )

(defn get-sayings
  ([db-spec] ((partial get-sayings-inner db-spec) identity))
  ([db-spec do-smth]
   ((partial get-sayings-inner db-spec) do-smth)))


(defn set-saying-inner [db say]
  (jdbc/with-db-connection [conn db]
    (jdbc/insert! conn "sayings" {"content" say})))

(defn set-saying [db-spec say] (set-saying-inner db-spec say))


(defn reset-saying
  ([db-spec]
   (jdbc/with-db-connection [conn db-spec]
     ;;(jdbc/delete! conn "sayings")
     (jdbc/execute! conn ["DELETE FROM sayings"]))))



(defn- clean-db [db-spec]
  (do (reset-saying db-spec)))

(defn init [db-spec]
  "Creates all db tables if they don't exist."
  (create-table-sayings-if-non-existant db-spec))
