(ns n-tools-log.init-log4j2
  (:require [n-tools-log.log4j2 :as tl]
            [example-server.env-vars :as envv :refer [my-env]]
            [clojure.tools.logging :as log]))

(defn testing-logging []
  ;;--------------------------------------------------------
  ;; For trying out logging
  ;; Example of using log4j2 (and slf4j)
  ;;
  ;; Logging levels:
  ;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
  ;;--------------------------------------------------------
  (log/info "**************************************************")
  (log/info "BEGINNING CLJ LOGGING TEST")
  (log/info "Some info msg.")
  (log/debug "Some debug msg.")
  (log/warn "Some warn msg.")
  (log/error "Some error msg.")
  (try (/ 1 0)
       (catch Exception e
         (log/error e "Catched exception!")))
  (log/fatal "Some fatal msg.")
  
  (log/debug "Spying expression returned: "
             (str "..."
                  (log/spy "Long sentence without ellipsis, once, with ellispis later.")
                  "..."))
  ;; And pretty print some:
  (log/infof " " {:a [1 2] :b [3 4]})
  
  ;; For trying out rolling file appender (before set in xml also SizeBasedTriggeringPolicy size to "1MB")
  (doseq [n (range 10)]
    (log/debug "Log msg number:" n))
  (log/info "END OF CLJ LOGGGING TEST")
  (log/info "**************************************************"))


;; Init logging:
(let [log-level (:log-level my-env)
      log-dir (:log-dir my-env)]
  ;; Expects log4j2 to have ${sys:log_directory} and ${sys:log_level}.
  (tl/setup-log-config :log-level (or log-level "debug")
                       :log-dir (or log-dir "/tmp/logs/n-cljs-examples/server_auth_jwt_buddy/")
                       :log4j2xml-sys-var-log-dir "log_directory"
                       :log4j2xml-sys-var-log-level "log_level")
  (log/info "Init log4j2 done!")
  (when (= log-level "debug")
    (testing-logging)))


#_(do (tl/setup-log-config :log-level "debug"
                         :log-dir "/tmp/logs/example-server/"
                         :log4j2xml-sys-var-log-dir "log_directory"
                         :log4j2xml-sys-var-log-level "log_level")
    (log/info "Init log4j2 done!"))

#_(comment
    "To be required as first dependency of the file with entry point, like:"
    (ns some-namespace
      (:require [n-tools-log.init-log4j2]
                ...))
  )
