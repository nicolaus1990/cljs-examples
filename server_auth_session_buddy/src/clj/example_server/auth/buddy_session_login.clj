(ns example-server.auth.buddy-session-login
  (:require [compojure.route :as route]
            [compojure.core :refer ;:all
                                   [GET POST routes]]
            [compojure.response :refer [render]]
            [ring.util.response :refer [response redirect]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.params :refer [wrap-params]]
            ;;Authorization and authentication
            [example-server.auth.login-page :as lp]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            ;;Debugging
            [example-server.routing.middleware.tools :refer [log-middleware]]
            [example-server.tools :refer [pformat]]
            [clojure.tools.logging :as log])
  ;(:gen-class)
  )

;; Default login and logout end-points.
(def path-url-login-default "/login")
(def path-url-logout-default "/logout")


(defn login
  ;; Login page controller
  ;; It returns a login page on get requests.
  [request]
  (render lp/login-page request))

;; Logout handler
;; Responsible for clearing the session.

(defn logout
  ([request] (logout request nil))
  ([request {:keys [path-url-login]
             :or {path-url-login  path-url-login-default}}]
   (-> (redirect path-url-login)
       (assoc :session {}))))

(defn failed-auth-page [request]
  (-> (render lp/error-403 request)
      (assoc :status 403)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Authentication                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Authentication Handler
;; Used to respond to POST requests to /login.



(defn get-login-authenticate
  "Check request username and password against authdata
  username and passwords.

  On successful authentication, set appropriate user
  into the session and redirect to the value of
  (:next (:query-params request)). On failed
  authentication, renders the login page."
  [authdata]
  (fn [request]
    (let [username (get-in request [:form-params "username"])
          password (get-in request [:form-params "password"])
          session (:session request)
          password-server-side (get authdata (keyword username))]
      ;;(log/debug (str "U: " username))
      ;;(log/debug (str "P: " password))
      ;;(log/debug (str "AD: " authdata))
      ;;(log/debug (str "PS: " password-server-side))
      (if (and password-server-side
               (= password-server-side
                  password))
        (let [;; If no next-url found redirect to home file.
              next-url (get-in request
                               [:query-params  "next" ;:next
                                ;; Parameter 'next' is not internalized as keyword
                                ] "/"
                               )
              ;; TODO: Why not give only username?
              updated-session (assoc session :identity (keyword (str username
                                                                     (:uri request))))]
          #_(log/debug (str "Request looks like this: " request))
          (log/debug (str "Redirecting to next-url: " next-url))
          (-> (redirect next-url);without context (url is absolute)
              (assoc :session updated-session)))
        (failed-auth-page request)))))

(defn auth-middleware [handler]
  (fn [req]
    (if-not (authenticated? req)
      (throw-unauthorized)
      (handler req))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routes and Middlewares                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; User defined application routes using compojure
;; routing library.
;; Note: We do not use middleware for authorization,
;; all of the authorization system is decoupled from
;; main routes.

(defn get-login-routes
  ([authdata] (get-login-routes authdata nil))
  ([authdata {:keys [path-url-login path-url-logout]
              :or {path-url-login  path-url-login-default
                   path-url-logout path-url-logout-default}
              :as options}]
   (let [login-authenticate (get-login-authenticate authdata)]
     (routes
      (GET  path-url-login  [] login)
      (POST path-url-login  [] login-authenticate)
      (GET  path-url-logout [] #(logout % options))))))


;; User defined unauthorized handler
;; This function is responsible for handling unauthorized requests (when
;; unauthorized exception is raised by some handler).
;; See: 'throw-unauthorized'

(defn unauthorized-handler
  [request metadata {:keys [path-url-login]
                     :or {path-url-login path-url-login-default}}]
  (cond
    ;; If request is authenticated, raise 403 instead
    ;; of 401 (because user is authenticated but permission
    ;; denied is raised).
    (authenticated? request)
    (do (log/debug "Authenticated but 403 status.")
        (-> (render lp/error-403 request)
            (assoc :status 403)))
    ;; In other cases, redirect the user to login page.
    :else
    (let [current-url (:uri request)]
      (log/debug (str "Redirecting to login page. " (format (str path-url-login "?next=%s") current-url)))
      (redirect (format (str path-url-login "?next=%s") current-url)))))

;; Create an instance of auth backend.

(def auth-backend-default
  (session-backend {:unauthorized-handler (fn [req metadata] (unauthorized-handler req metadata nil))}))

(defn create-new-auth-backend [options]
  (session-backend {:unauthorized-handler (fn [req metadata] (unauthorized-handler req metadata options))}))



(defn secure-handler
  ([route-to-secure auth-data base-url]
   (secure-handler route-to-secure
                   auth-data
                   base-url
                   ;;auth-backend
                   {:auth-backend auth-backend-default}))
  ([route-to-secure auth-data base-url {:keys [auth-backend path-url-login path-url-logout]
                                        :as options}]
   ;; base-url is something like: "/folderName"
   ;; Authdata should look like this: {:admin "secret"}, where 'admin' is username.
   (as-> route-to-secure $
     ;;(wrap-resource $ root-folder)
     ;; Additional wrappers for wrap-resource (more information:
     ;; https://github.com/ring-clojure/ring/wiki/Static-Resources
     ;;(wrap-content-type $)
     ;;(wrap-not-modified $)
     (auth-middleware $)
     ;;GET wrapper has to be at the end. 
     (GET (str base-url "*") [] $)
     ;;(GET (str "/n_app_lists" "*") [] $)
     ;;
     (routes $ (get-login-routes auth-data options))
     (wrap-authorization $ auth-backend)
     (wrap-authentication $ auth-backend)
     (wrap-params $)
     (wrap-session $)
     (log-middleware $ "Request for private files: "
                     "Response buddy (private files):")
     )))

;; (def route-home
;;   (let [route (route/resources base-url
;;                                {:root root-folder})]
;;     (as-> route $

;;       (first-link-fix-middleware $)
;;       (b/auth-middleware $)
;;       ;;GET wrapper has to be at the end. 
;;       (GET (str base-url "*") [] $)
;;       )))
