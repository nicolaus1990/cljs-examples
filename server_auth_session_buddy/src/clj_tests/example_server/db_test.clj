(ns example-server.db-test
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            ;;[example-server.db.db-admin :refer [get-authdata]]
            [example-server.db :as dba]
            [example-server.test-db-setup :refer [*txn* always-rollback]]))

;; ---------------------------------
;; ------------- Setup db
;; ---------------------------------


(use-fixtures :each always-rollback)

(def user-pwd {:username "user_test" :password "password_test"})
(defn insert-dummy-data [db]
  (jdbc/insert! db :my_users user-pwd))

(defn setup-db [db]
  (dba/create-table-users-if-non-existant db)
  (insert-dummy-data db))

;; (jdbc/db-do-commands
;;  db true
;;  ["drop table if exists my_users"
;;   "drop table if exists sayings"
;;   (jdbc/create-table-ddl :my_users
;;                          [[:id "SERIAL" :primary "KEY"]
;;                           [:username "VARCHAR(64)" :not "NULL"]
;;                           [:password "VARCHAR(64)" :not "NULL"]]
;;                          ;; {:table-spec "ENGINE=InnoDB"
;;                          ;;  :entities clojure.string/upper-case}
;;                          )
;;   (jdbc/create-table-ddl :sayings
;;                          [[:id "SERIAL" :primary "KEY"]
;;                           [:content "VARCHAR(100)"]])])

;; ---------------------------------
;; ------------- Tests
;; ---------------------------------


(def expected-table
  (str
   "CREATE TABLE my_users ("
   "id SERIAL primary KEY, "
   "username VARCHAR(64) not NULL, "
   "password VARCHAR(64) not NULL"
   ")"))

(deftest check-sql-create-table-str
  (testing "Check create table sql expression"
    (is (= expected-table dba/create-table-users-str))))


(def user-pwd-ans {:user_test "password_test"})
(def user-pwd-ans2 {:nikki "secret" :user_test "password_test"})

(deftest check-authdata
  (setup-db *txn*)
  (testing "Checking authorization data"
    (is (= user-pwd-ans2 (dba/get-authdata *txn*)))))
