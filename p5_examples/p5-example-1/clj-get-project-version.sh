#!/bin/bash

function echo_help () {
    echo "  ---   n-clj-get-project-version.sh  ---  ";
    echo "Returns version number in lein's project.clj file.";
    echo "Parameters: ";
    echo "     1st: path of project.clj file (example: ./some/path/project.clj).";
}

if [[ -z "$1" ]];
then
    echo_help ;
    exit 1;
fi

if ! [[ $1 = *project.clj ]]; then echo "Validation: First arg should be a /some/path/project.clj file."; echo_help; exit 1; fi

# Get first line of project.clj file
line=$(head -n 1 $1)
if ! [[ $line =~ ^\(defproject[[:space:]]+[a-z\-]+[[:space:]]+\"[0-9]+.[0-9]+.[0-9]+\"[[:space:]]*$ ]]; then
    echo "Validation: First line needs to match '(defproject <proj-name-small-caps> \"1.2.345\"'";
    exit 1;
fi

#This is similar to this: version_num_with_quotes=$(echo $line | cut -d' ' -f 3)
# "(defproject some-name "1.2.34" " -> "1.2.34"
version_num_with_quotes=$(cut -d' ' -f 3 <<< $line)

# Length of version number with quotes
# "1.2.34" -> 8
len_version_num_with_quotes=$(wc -m <<< $version_num_with_quotes)

# Decrement twice to get index before last quote.
# 8 -> 6
index_before_last_quote=$(($len_version_num_with_quotes-2))

# "1.2.34" -> 1.2.34
version_num_without_quotes=$(echo $version_num_with_quotes | cut -c2-$index_before_last_quote)

if [[ $version_num_without_quotes =~ ^[0-9]+.[0-9]+.[0-9]+$ ]]; then
    echo $version_num_without_quotes
    exit 0;
else
    echo "Something went wrong! Vars:"
    echo "Line:                                 $line" ;
    echo "Version num with quotes:              $version_num_with_quotes";
    echo "Length of version num with quotes:    $len_version_num_with_quotes";
    echo "Index before last quote:              $index_before_last_quote";
    echo "Version num without quotes:           $version_num_without_quotes";
    echo_help;
    exit 1;
fi
