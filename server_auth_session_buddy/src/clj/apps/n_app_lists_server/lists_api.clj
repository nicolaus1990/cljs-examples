(ns apps.n-app-lists-server.lists-api
  (:require ;;These ring deps came (probably?) with figwheel.
            #_[ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.defaults :as rmid]
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [apps.n-app-lists-server.db :as db]
            ;;[compojure.route :as route]
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc]
            [clojure.tools.logging :as log]
            [clojure.set :as cset]))


(defn respond-error-msg
  ([msg] (respond-error-msg msg 500))
  ([msg status]
   (-> {:message msg}
       (response)
       (status status))))


(defroutes api-lists-inner
  (context "/n_app_lists/api" []
           (GET "/lists" req
                (log/debug "--- REQ: " req)
                (let [xss (db/db->list)]
                  (log/debug "*** SENDING BACK LIST: " xss)
                  (response xss)))
           ;; If id and depth given, return list with sublists up to depth loaded.
           (GET "/list/:id" [id]
                (try
                  (log/debug "--- REQ (PARTIAL LIST id and depth): id: " id)
                  (let [xss (db/get-list id)]
                    (log/debug "*** SENDING BACK PARTIAL LIST: " xss)
                    (response xss))
                  (catch Exception ex
                    (log/error (str "Getting partial lists failed. " (.getMessage ex)) ex)
                    (respond-error-msg "Server error: Getting partial list failed."))))
           ;; POST modifes, inserts and removes lists (-items).
           (POST "/lists" req
                 (if-let [body (:body req)]
                   (try 
                     (let [flat-lists (json/read-str (slurp body) :key-fn keyword)]
                       (do (log/debug "--- REQ: " req)
                           (log/debug "--- BODY: " body)
                           (log/debug "-- LISTS: " flat-lists)
                           (-> (db/flat-list->db db/db-spec flat-lists)
                               (response ))))
                     (catch Exception ex
                       (log/error (str "Saving lists failed. " (.getMessage ex)) ex)
                       (respond-error-msg "Server error: Getting link titles failed.")))
                   (respond-error-msg "Client error (while deleting): Body (with lists-ids data) came empty."
                                        400)))))


(def api-lists (wrap-json-response api-lists-inner))
