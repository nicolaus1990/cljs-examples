(ns example-org-js.org-js-fns
  (:require
   [goog.dom :as dom]
   [goog.dom.TagName :as TagName]
   ;;[goog.events :as evts]
   [org-js]
   [example-org-js.tools :as t]
   [oops.core :refer [oget oset! ocall oapply ocall! oapply!
                      oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]))

(defn log-org-html-doc [doc]
  (t/log "*** Logging org-html-doc: ")
  (.dir js/console doc)
  (t/log (.-toString doc)))

;; var orgCode = "some org notation text";
;; var orgParser = new Org.Parser();
;; var orgDocument = orgParser.parse(orgCode);
;; var orgHTMLDocument = orgDocument.convert(Org.ConverterHTML, {
;;   headerOffset: 1,
;;   exportFromLineNumber: false,
;;   suppressSubScriptHandling: false,
;;   suppressAutoLink: false
;; });
;; console.dir(orgHTMLDocument); // => { title, contentHTML, tocHTML, toc }
;; console.log(orgHTMLDocument.toString()) // => Rendered HTML
(def example-org-code "#+Title: Demo org.js

org.js is a parser and converter for org-mode ([[http://orgmode.org/]]) notation written in JavaScript.

* Installation

** Node.js

Install ~org-js~ module via ~npm~.

#+BEGIN_EXAMPLE
npm install org
#+END_EXAMPLE

Then, in your application, load installed module as follows.

#+BEGIN_SRC 
var Org = require(\"org\");
#+END_SRC

** Web Browser

Just place ~org.js~ in your website and load it.

#+BEGIN_SRC html
<script type=\"text/javascript\" src=\"org.js\"></script>
#+END_SRC

* Usage

** Org -> HTML conversion

#+BEGIN_SRC javascript
var orgCode = \"some org notation text\";
var orgParser = new Org.Parser();
var orgDocument = orgParser.parse(orgCode);
var orgHTMLDocument = orgDocument.convert(Org.ConverterHTML, {
  headerOffset: 1,
  exportFromLineNumber: false,
  suppressSubScriptHandling: false,
  suppressAutoLink: false
});

console.dir(orgHTMLDocument); // => { title, contentHTML, tocHTML, toc }
console.log(orgHTMLDocument.toString()) // => Rendered HTML
#+END_SRC

* Supported notations
- One 
    - Histeria
- Two
  - Five 
  - Three
    - Four
** Blocks

*** Table

|-------+--------+------------|
|       | Symbol | Author     |
|-------+--------+------------|
| Emacs | ~M-x~  | _RMS_      |
|-------+--------+------------|
| Vi    | ~:~    | _Bill Joy_ |
|-------+--------+------------|

*** List

**** Ordered List

1. Orange
2. Banana
3. Apple

**** Unordered List

- Foo
- Bar
- Baz

**** Definition List

- vim :: Vi IMproved, a programmers text editor
- ed :: Line-oriented text editor

*** Rule

-----

*** Directive

**** ~BEGIN_QUOTE~ and ~END_QUOTE~

#+BEGIN_QUOTE
To be or not to be, that is the question.
#+END_QUOTE

**** ~BEGIN_EXAMPLE~ and ~END_EXAMPLE~

#+BEGIN_EXAMPLE
npm install org
#+END_EXAMPLE

**** ~BEGIN_SRC~ and ~END_SRC~

#+BEGIN_SRC javascript
var Org = require(\"org\");
var orgParser = new Org.Parser();
#+END_SRC

** Inline elements

*** Bold

GNU is *Not* Unix.

*** Italic

GNU is /Not/ Unix.

*** Code

Try =cat= or ~tac~.

*** Dashed

+Deleted+

*** Subscript

Sub_{script}. a_{1}, a_{2}, and a_{3}.

** Handling TODO

** TODO Implement todo attributes

Support attributes like ~SCHEDULED:~.

** DONE Implement todo heading

Parse ~TODO/DONE~ in headers.

* Directive support

- ~options:~
- ~title:~
- ~author:~
- ~email:~
")

;; Some native js vars and functions were not needed to call with oops-lib,
;; probably because clojurescript compiler is infering externs.
(def Parser (let [Parser-constructor (.-Parser js/Org)] (Parser-constructor.)))

(def example-org-doc (. Parser (parse example-org-code)))


(def ConverterHTML (.-ConverterHTML js/Org))

(def example-org-html-doc
  ;; Note: this does not survive advanced compilation:
  #_(.convert example-org-doc ConverterHTML
           (clj->js {:headerOffset 1
                     :exportFromLineNumber false
                     :suppressSubScriptHandling false
                     :suppressAutoLink false
                     }))
  ;; This, with the help of oops-lib, does:
  (ocall example-org-doc "convert" ConverterHTML
           (clj->js {:headerOffset 1
                     :exportFromLineNumber false
                     :suppressSubScriptHandling false
                     :suppressAutoLink false
                     })))

(defn set-org! [node org-html-doc]
  ;; Note: Demo is outdated
  #_(set! (.-innerHTML node)
          (.. js/Org -HTMLTextConverter (convertDocument example-org-doc)))
  ;; This only works if node was selected with jquery
  #_(.html node example-org-html-doc)
  (set! (.-innerHTML node)
        org-html-doc))

(defn insert-org-editor-html-nodes [node]
  ;; Will insert inital nodes like org-js example (with minor modifications).
  ;; If node is div node with id "app", then it should insert nodes such that:
  ;; [:div#app [:div#container
  ;;            [:div#input-container]
  ;;            [:div#result-container [:div#result]]]]
  ;; Returns new created nodes in map.
  (let [div-container        (dom/createElement TagName/DIV)
        div-input-container  (dom/createElement TagName/DIV)
        div-result-container (dom/createElement TagName/DIV)
        div-result           (dom/createElement TagName/DIV)]
    (doto div-result-container  (dom/setProperties #js {:id "result-container"}))
    ;; CSS of readtheorg needs div with id content (previously div node with result
    (doto div-result            (dom/setProperties #js {:id "content" :class "result"}))
    (doto div-input-container   (dom/setProperties #js {:id "input-container"}))
    (doto div-container         (dom/setProperties #js {:id "container"} #_{:id id :style "width: 350px"})
          #_(dom/setTextContent "Text: "))
    (dom/insertChildAt  div-result-container div-result 0)
    (dom/insertChildAt  div-container div-input-container 0)
    (dom/insertChildAt  div-container div-result-container 1)
    (dom/insertChildAt  node div-container 0)
    {:div-container        div-container
     :div-input-container  div-input-container
     :div-result-container div-result-container
     :div-result           div-result }))


