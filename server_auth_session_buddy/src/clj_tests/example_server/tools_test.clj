(ns example-server.tools-test
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [example-server.tools :as dbt]
            [example-server.env-vars :refer [my-env]]))

;;;;;;; Preliminary

(def db
  ;; Should be:
  ;; {:classname "org.postgresql.Driver"
  ;;  :subprotocol "postgresql"
  ;;  :subname "//localhost:5432/nikkis_server_test"
  ;;  :user "nikki"
  ;;  :password "secret"}
  (:db-spec my-env))

;;;;; Fixtures

(declare ^:dynamic *txn*)

(defn once-fixture [f]
  ;;(println "setup once")
  (f)
  ;;(println "teardown once")
  )
(defn always-rollback [f]
  (do ;;(println "setup each")
      (jdbc/with-db-transaction
        [transaction db]
        ;;From doc of 'db-set-rollback-only!': "Marks the outermost transaction
        ;;such that it will rollback rather than commit when complete."
        (jdbc/db-set-rollback-only! transaction)
        (binding [*txn* transaction] (f)))
      ;;(println "teardown each")
      ))
;; (defn each-fixture [f]
;;   ;;(println "setup each")
;;   (f)
;;   ;;(println "teardown each")
;;)
(use-fixtures :each ;;each-fixture
              always-rollback)
;;(use-fixtures :once once-fixture)

;;;;;; Set-up test database:

(defn create-some-table [db-spec]
  (do
    (jdbc/db-do-commands
     db-spec true
     ["drop table if exists some_table"
      (jdbc/create-table-ddl :some_table
                             [[:id "SERIAL" :primary "KEY"]
                              [:attr1 "VARCHAR(64)" :not "NULL"]]
                             ;; {:table-spec "ENGINE=InnoDB"
                             ;;  :entities clojure.string/upper-case}
                             )])
    (jdbc/insert! db-spec :some_table {:attr1 "Some value"})))

;;;;; Tests

(deftest table-exists?-test
  (create-some-table *txn*)
  (testing "Checking table-exists? fn"
    (is (= true (dbt/table-exists? *txn* "some_table"))
        (= false (dbt/table-exists? *txn* "Some_non_existing_table")))))




