(ns apps.n-app-lists-server.db
  (:require ;;These ring deps came (probably?) with figwheel.
            #_[ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.middleware.defaults :as rmid]
            [ring.util.response :refer [response status resource-response content-type not-found]]
            ;;[compojure.route :as route]
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [clojure.string :as clj-str]
            [clojure.set :as cset]))

(def tn-list "n_app_lists_lists")

(def id-root-list-node "APP_ROOT_L")
(def root-list-node-content "All lists:")

(defn my-rand-str
  ([] (my-rand-str 10))
  ([len]
   (apply str (take len (repeatedly #(char (+ (rand 26) 65)))))))

(def example-list
  {:id id-root-list-node :x root-list-node-content
   :xs [{:id (my-rand-str) :x "Hi from cljc!" :xs nil}
        {:id (my-rand-str) :x "List 1" :xs [{:id (my-rand-str) :x "List 1.1"
                                             :xs [{:id (my-rand-str) :x "List 1.1.1"}
                                                  {:id (my-rand-str) :x "List 1.1.2"}]}]}
        {:id (my-rand-str) :x "List 2" :xs [{:id (my-rand-str) :x "List 2.1"
                                             :xs [{:id (my-rand-str) :x "List 2.1.1"}
                                                  {:id (my-rand-str) :x "List 2.1.2" :xs [{:id (my-rand-str) :x "List 2.1.2.1"}
                                                                                          {:id (my-rand-str) :x "List 2.1.2.2"}]}]}]}]})

;;---------------------------------------------------------------
;; SQL-DB
;;---------------------------------------------------------------

;;(declare db-spec)

(comment
  "*********** Postgres: ***********
- sudo -u postgres psql
- sudo -u postgres -i
- CREATE DATABASE nm_server_test;
- \\c nm_server_test ;
- databases: 'psql postgres' and \\l ;To see dbs
- \\dt ;To see tables
- \\di ;To see indeces
- psql nm_server_test ;connect to db
- To load (or restart) database, load sql files (at root level).
- Useful queries: 
  - SELECT id,sublist_of,sublist_pos,x,link_title FROM n_app_lists_lists ORDER BY sublist_of, sublist_pos ASC;
"
  )

(def ^:dynamic db-spec
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname "//localhost:5432/nm_server_test"
   :user "nm_test"
   :password "nm_test"})

;; For when using heroku
(defn set-db! [db]
  #_(set! db-spec db)
  (alter-var-root (var db-spec) (fn [old-spec] db)))


(defn db->list []
  (log/debug "db->list called")
  example-list)

(defn get-list [id]
  (log/debug "get-list called")
  false)

(defn flat-list->db [db-spec flat-lists]
  (log/debug "flat-list->db called")
  false)

(defn db-init [& {:keys [db-spec dynamic-set-db
                         drop-if-exists drop-records
                         spit-backup
                         load-edns]
                  :or {db-spec db-spec,  dynamic-set-db false,
                       drop-if-exists false, drop-records false,
                       spit-backup nil
                       load-edns false}
                  :as all}]
  "Will backup db, i.e. will write sql table as csv files every
  number of `spit-backup` days, if given.

Can also load edn files to backup table."
  (when dynamic-set-db
    (log/debug "Dynamically setting DB spec.")
    (set-db! db-spec))
  (log/debug "TODO: Create tables and indeces"))



(defn insert-dummy-data []
  (log/debug "Dev: Inserting some dummy values for list app")
  nil)



