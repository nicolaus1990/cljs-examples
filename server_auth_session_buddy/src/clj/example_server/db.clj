(ns example-server.db
  (:require [clojure.java.jdbc :as jdbc]
            ;;Environment variables
            [example-server.env-vars :refer [my-env]]
            [example-server.tools :as dbt]
            [clojure.tools.logging :as log])
  ;(:gen-class)
  )

;;--------------------------------------------------------
;; Db-spec
;;--------------------------------------------------------

(defn get-db-spec []
  (or (my-env :database-url)
      ;; Should be:
      ;; {:classname "org.postgresql.Driver"
      ;;  :subprotocol "postgresql"
      ;;  :subname "//localhost:5432/nikkis_server_test"
      ;;  :user "nikki"
      ;;  :password "secret"}
      (my-env :db-spec))
  ;; (env :db-spec
  ;;      ;;Or heroku:
  ;;      (env :database-url))
  )

#_(comment
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname "//localhost:5432/nm_server_test"
   :user "nm_test"
   :password "nm_test"}
  )


;;--------------------------------------------------------
;; DB-admin
;;--------------------------------------------------------

(def table-name "my_users")

;; CREATE TABLE my_users
;; (
;;  id SERIAL PRIMARY KEY,
;;  username VARCHAR(64) NOT NULL UNIQUE,
;;  password VARCHAR(64) NOT NULL
;; );
;; INSERT INTO my_users (username,password) VALUES ('', '');
(def create-table-users-str
  (jdbc/create-table-ddl (keyword table-name) ;;:my_users
                         [[:id "SERIAL" :primary "KEY"]
                          [:username "VARCHAR(64)" :not "NULL"]
                          [:password "VARCHAR(64)" :not "NULL"]]
                         ;; {:table-spec "ENGINE=InnoDB"
                         ;;  :entities clojure.string/upper-case}
                         ))


;; (defn create-table-users
;;   ([db-spec] (create-table-users db-spec false false))
;;   ([db-spec drop-if-exists only-if-non-existant]
;;    (if drop-if-exists
;;      (let [sql-exprs [(str  "drop table if exists " table-name)
;;                       create-table-users-str]]
;;        (jdbc/db-do-commands db-spec true sql-exprs))
;;      (let [sql-exprs [create-table-users-str]]
;;        (if only-if-non-existant
;;          (if-not (table-exists? db-spec table-name)
;;            (jdbc/db-do-commands db-spec true sql-exprs)
;;            (log/debug "Table " table-name " exists already."))
;;          (jdbc/db-do-commands db-spec true sql-exprs)))
;;      )))

;; (defn create-table-users-if-non-existant [db-spec]
;;   (create-table-users db-spec false true))

(defn create-table-users-if-non-existant [db-spec]
  (dbt/create-table :db-spec db-spec
                    :table-name table-name
                    :ddl create-table-users-str
                    :drop-if-exists false))

;; Authdata should look like this: {:admin "secret"}, where 'admin' is the
;; username. (i.e. : keyword: username, value the password).
(defn get-authdata
  "Usage (for example):

(get-authdata   \"jdbc:postgres://USER:PASSWORD@localhost:5432/DB_NAME\") --> 
{:user_name_1 \"password_u1\"
 :user_name_2 \"password_u2\"}
  "
  ([] (get-authdata (get-db-spec)))
  ([db-spec]
   (let [lseq (jdbc/with-db-connection [conn db-spec]
                (jdbc/query conn [(str "select username, password from "
                                       table-name)]))]
     (reduce (fn [acc user] (assoc acc
                                   (keyword  (:username user))
                                   (:password user)))
             {}
             lseq))))


;;--------------------------------------------------------
;; DB init
;;--------------------------------------------------------

(defn init []
  "Creates all db tables if they don't exist."
  (create-table-users-if-non-existant (get-db-spec)))
