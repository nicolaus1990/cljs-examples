(ns n-tools-log.example-server-dev
  (:require [n-tools-log.init-log4j2]
            [example-server.web :as web]
            [clojure.tools.logging :as log]))


(defn -main [& args]
  (do (log/info "*********** DEBUG MODE ***********")
      (apply web/-main args)))
