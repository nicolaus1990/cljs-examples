(ns example-server.env-vars
  (:require [environ.core :refer [env]]))

(defn my-boolean-coerce [s]
  (if (boolean? s) s
      (= s "true")))

(defn give-nil-if-empty-str [s]
  (if (= "" s) nil s))

;;; Environ plugin casts everything to string
(def my-env
  "My environment variables, without the port (because port could possibly be
  given as command-line argument)."
  {;;:port (Integer. (or (env :port) 5000))
   :production (my-boolean-coerce (env :production true))
   :serverUrl (env :serverUrl "https://nikkis-stuff.herokuapp.com")
   ;;Logging:
   :log-dir (env :log-dir nil);;(env :log-dir "/tmp/logs/example-server/")
   :log-level (env :log-level nil) ;;(env :log-level "debug") ;;(keyword (env :log-level :debug))
   :log-write-to-file (my-boolean-coerce (env :log-write-to-file true))
   ;;Database:
   :database-url (give-nil-if-empty-str (env :database-url))
   :db-spec {:classname   (or (env :db-spec-classname)
                              "org.postgresql.Driver") 
             :subprotocol (or (env :db-spec-subprotocol)
                              "postgresql")
             :subname     (or (env :db-spec-subname)
                              "//localhost:5432/nm_server_test")
             :user        (or (env :db-spec-user)
                              "nm_test")
             :password    (or (env :db-spec-password)
                              "nm_test")}
   })

(defn establish-port
  ([] (establish-port nil))
  ([port] (Integer. (or port (env :port) 3000))))


