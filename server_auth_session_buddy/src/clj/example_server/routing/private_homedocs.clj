(ns example-server.routing.private-homedocs
  (:require [clojure.java.io :as io]
            ;;Webdev
            [ring.util.response :refer [redirect]]
            [ring.middleware.defaults :as rmid]
            ;;[ring.middleware.file :as rfile]
            [compojure.route :as route]
            [compojure.core :refer ;:all
                                   [GET routes]]
            ;;Authorization and authentication
            [example-server.auth.buddy-session-login :as b]
            [example-server.db :as db]
            ;;Logging:
            [clojure.tools.logging :as log]))

;; (def route-name "homedocs")
;; (def base-url (str "/" route-name))
;; (def root-folder (str  "private/" route-name "/"))

(defn first-link-fix-middleware [handler {:keys [route-name base-url root-folder]}]
  ;; This middleware could be prevented: It's just because we're redirecting
  ;; with "/homedocs" in "login-authenticate".
  (fn [req] 
    (if (or  (= (:uri req) base-url)
             (= (:uri req) (str base-url "/")))
      ;;We have to redirect and not just update uri in request (because for some
      ;;unknown reason it returns the html page but without the 'homedocs'
      ;;context, i.e.: links of file point to '/filename.html' and not to
      ;;'/homedocs/filename.html', for example).
      ;(handler (update req :uri (fn [_]  "/homedocs/index.html")))
      (redirect (str base-url "/index.html"))
      (handler req))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routes and Middlewares                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn get-handler [{:keys [route-name base-url root-folder full-path-to-folder] :as route-data}]
  (let [route #_(route/resources base-url
                                 {:root ;;root-folder
                                  full-path-to-folder
                                  })
        (route/files full-path-to-folder)
        #_(route/resources "/")]
    (as-> route $
      (first-link-fix-middleware $ route-data)
      (rmid/wrap-defaults $ (-> rmid/site-defaults
                                (assoc-in [:security :anti-forgery] false)
                                (assoc-in [:static :files] [full-path-to-folder])
                                #_(assoc-in [:session :cookie-name] route-name)))
      (b/secure-handler $ (db/get-authdata) base-url (log/spy route-data)))))

(def routes-data-default
  ;; Basically every route is a folder in private (secured) folder.
  [
   {:route-name "n_app_lists" :base-url "/n_app_lists" :root-folder "private/n_app_lists/"}
   {:route-name "homedocs"    :base-url "/homedocs"    :root-folder "private/homedocs/"}
   
   ])

(defn get-all-handlers-in-one
  ([STATIC-HTML-FOLDER-PRIVATE] (get-all-handlers-in-one STATIC-HTML-FOLDER-PRIVATE routes-data-default))
  ([STATIC-HTML-FOLDER-PRIVATE routes-data]
   ;;(reduce #(routes %1 %2))
   (apply routes
          (for [route-data routes-data]
            (let [r-data (-> route-data
                             ;; Will add /login and /logut end-point names (strings).
                             (assoc :full-path-to-folder STATIC-HTML-FOLDER-PRIVATE)
                             (assoc :path-url-login      (str "/login_"  (:route-name route-data))
                                    :path-url-logout     (str "/logout_" (:route-name route-data))))]
              ;; Note: Creation of auth-backend needs keys path-url-login/logout
              (-> (assoc r-data :auth-backend (b/create-new-auth-backend r-data))
                  log/spy
                  (get-handler)))))))
