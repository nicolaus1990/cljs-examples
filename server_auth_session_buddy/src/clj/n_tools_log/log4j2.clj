(ns n-tools-log.log4j2
  (:require [clojure.tools.logging :as log])
  (:import [org.apache.logging.log4j LogManager]
           [org.apache.logging.log4j Level]
           [org.apache.logging.log4j.core.config Configuration]
           [org.apache.logging.log4j.core.config LoggerConfig]
           [org.slf4j Logger] 
           [org.slf4j LoggerFactory]
           [java.io IOException]
           [java.lang.invoke MethodHandles]))


(defn setup-log-config [& {:keys [log-dir log-level 
                                  log4j2xml-sys-var-log-dir 
                                  log4j2xml-sys-var-log-level]
                           :or {log-dir "/tmp/logs/"
                                log-level "all"
                                log4j2xml-sys-var-log-dir "log_directory"
                                log4j2xml-sys-var-log-level "log_level"}}]
  "Sets log_directory and log_level variables and use rolling file appender in root logger 
node in 'log4j2.xml'.
" 
  (try
    (System/setProperty log4j2xml-sys-var-log-dir log-dir)
    (System/setProperty log4j2xml-sys-var-log-level log-level)
    (let* [logger (LoggerFactory/getLogger (.. MethodHandles lookup lookupClass))
           ;;ctx (LogManager/getContext false)
           ;;_ (. ctx reconfigure)
           ;;config (. ctx getConfiguration)
           ;;logger-config (. config getLoggerConfig LogManager/ROOT_LOGGER_NAME)
           ]
      (log/info "Logging initialized."))
    (catch IOException e
      (println "Failed configuring log4j2 (function n-tools-log.log4j2/setup-logger.")
      (throw e))))




(comment 
  "Translated from java:"

;;   package Tools;
;;
;; import org.apache.logging.log4j.Level;
;; import org.apache.logging.log4j.LogManager;
;; import org.apache.logging.log4j.core.config.Configuration;
;; import org.apache.logging.log4j.core.config.LoggerConfig;
;; import org.slf4j.Logger;
;; import org.slf4j.LoggerFactory;
;;
;; import java.io.IOException;
;; import java.lang.invoke.MethodHandles;
;;
;; /*
;;  * For logging either:
;;  *       i)  set logDirectory variable and use rolling file appender in root logger
;;  *         node in 'log4j2_OLD.xml', or...
;;  *     ii) create appender programmatically. (NOT IMPLEMENTED)
;;  */
;; public class LoggingTools {
;;
;;     private static Logger logger;
;;
;;     /*
;;      * SLF4J with log4j stuff
;;      */
;;     /*
;;      * Assumes that in log4j2_OLD.xml file has sys:logDirectory (like: ${sys:logDirectory}) and
;;      * sys:logLevel.
;;      * After invoking this method, you can instantiate the logger with:
;;      *      logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
;;      */
;;     public static void initLogging(String logDir, String logSuffix, boolean debugMode){
;;         System.setProperty("logDirectory", logDir);
;;         System.setProperty("logSuffix", logSuffix);
;;         if (debugMode){
;;             System.setProperty("logLevel", "DEBUG");
;;         } else {
;;             System.setProperty("logLevel", "INFO");
;;         }

;;         logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
;;         logger.debug("Logging initilized");
;;     }
;; }
  )
