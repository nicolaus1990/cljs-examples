(ns apps.things.db-test
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            ;;[example-server.db.db-admin :refer [get-authdata]]
            [apps.things.db :as dbl]
            [example-server.test-db-setup :refer [*txn* always-rollback]]))

;; ---------------------------------
;; ------------- Setup db
;; ---------------------------------


(use-fixtures :each always-rollback)
;;(use-fixtures :once once-fixture)


(def dummy-sayings
  [{:content"Ant"}
   {:content "Beaver"}
   {:content "Camel"}
   {:content "Dinosaur"}                        
   {:content "Elephant"}
   {:content "Frog"}])

(defn insert-dummy-data [db]
  (jdbc/insert-multi! db :sayings
                      dummy-sayings))

(defn setup-db [db]
  (dbl/create-table-sayings-if-non-existant db)
  (insert-dummy-data db))

;;;;;;;; Tests

(deftest get-sayings-inner-test
  (setup-db *txn*)
  (testing "Checking fn get-sayings"
    (is (every? (fn [s] (some #{s} (dbl/get-sayings-inner *txn* identity)))
                dummy-sayings))
    ;;Could be inserting dummy data in user-experience-test, so better not
    ;;check if they have same amount of elems.
    ;;(is (= (count dummy-sayings) (count (dbl/get-sayings-inner *txn* identity))))
    (let [prefix "Animals: "
          postfix "!!!"
          sayings (dbl/get-sayings-inner *txn*
                                   #(format (str prefix "%s" postfix)
                                            (:content %)))]
      (is (some #{(str prefix "Ant" postfix)} sayings))
      (is (every?
           (fn  [d] (some #(= % (str prefix (:content d) postfix))
                          sayings))
           dummy-sayings)
                   ))))

