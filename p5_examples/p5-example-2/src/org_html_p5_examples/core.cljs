(ns ^:figwheel-no-load org-html-p5-examples.core
  (:require [org-html-p5-examples.tools :as t :refer [rand-str log log-ind error alert]]
            [org-html-p5-examples.config :as config]
            [org-html-p5-examples.p5-example2 :as p5-ex2]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
;;(enable-console-print!)
;; See org-html-p5-examples.config

(println "This text is printed from src/org-html-p5-examples/core.cljs. Edit it and see reloading in action!")

(defn dev-setup []
  "Function to setup whatever it needs for development"
  (when config/debug?
    (println "Development mode is activated!")
    ;;(devtools/install!)
    (do
      (js/console.log "Trying out if npm external (non-cljsjs, non-closure  modules) deps are loaded. ")
      ;; moment.js
      #_(do  (js/console.log "Testing moment library.")
           (js/console.log  (str "Requires: moment.js (\"$ npm add moment\" and "
                                 "in this file \"(:require [moment])\")."))
           (js/console.log moment)
           (js/console.log (str "Hello there it's moment.js loaded with npm and webpack. Time of day: "
                                (.format (moment) "dddd"))))
      )))



(defn ^:export start-up []
  "This start-up fn has meta-data :export, which means, can be run in html (js) with: 
<script>org-html-p5-examples.core.start_up();</script>"
  #_(dev-setup)
  #_(run-injectSearchBar)
  #_(ho/run)
  #_(li/run)
  ;;(csb/run)
  (p5-ex2/run)
  )

(js/setTimeout start-up 1000)








