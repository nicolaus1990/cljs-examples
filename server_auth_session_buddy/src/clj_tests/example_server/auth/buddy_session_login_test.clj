(ns example-server.auth.buddy-session-login-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [peridot.core :as pd]
            [clojure.java.io :as io]
            [compojure.core :refer :all]
            [example-server.auth.buddy-session-login :as b]
            [example-server.tools :refer [pformat]]))

(def someLockedUrl "/folderName")
(def user "someUserName")
(def password "someUserNamesPassword")
(def dummy-authdata {(keyword user) password})
(def securedPageBodyContent "Hello!")

(def params {:username user
             :password password})
(def form-params {"username" user
                  "password" password})

(defn dummy-hello [req] {:body securedPageBodyContent})
(defroutes dummy-route
  (GET someLockedUrl req dummy-hello)
  (ANY  "*" [] {:status 404 :body "Not found"}))

(def buddy-wrap (-> dummy-route b/auth-middleware))
(def buddy-app (b/secure-handler buddy-wrap
                                 dummy-authdata
                                 someLockedUrl))

(defn js-hash-fn-available []
  (testing "Is sha256 js function reachable? Is it at root resource folder?"
    (is (not (= nil (io/resource "sha256-function.js"))))))

;; (def successfulLoginReq
;;   {:form-params {"username" "aUserName"
;;                  "password" "aUserPwd"}})
;; (deftest buddy-login-fail
;;   (let* [peridot-response (-> (pd/session buddy-app)
;;                               (pd/request "/")
;;                               (pd/follow-redirect))
;;          response (:response peridot-response)]
;;     (println (pformat peridot-response))
;;     (println (pformat response))
;;     (is (= 200 (:status response)))
;;     (is (= "Hello!"
;;            (get-in response [:body])))
;;     )
;;   )

(defn buddy-login-fail []
  (testing "Login failed."
    (let* [peridot-response ;;(buddy-app (mock/request :get someLockedUrl))
           (-> (pd/session buddy-app)
               (pd/request someLockedUrl)
               (pd/follow-redirect)
               ;;(pd/request "login-buddy/next?=/" :request-method :post)
               (pd/request someLockedUrl
                           :uri "/login-buddy"
                           :request-method :post
                           :form-params {"username" "wrongUser"
                                         "password" "wrongPassword"}
                           :query-params {"next" "/"}
                           ))
           response (:response peridot-response)]
      (println (pformat peridot-response))
      (println (pformat response))
      (is (= 400 (:status response))))))


(defn buddy-login []
  (testing "Login"
    (let* [peridot-response ;;(buddy-app (mock/request :get someLockedUrl))
           (-> (pd/session buddy-app)
               (pd/request someLockedUrl)
               (pd/follow-redirect)
               ;;(pd/authorize "aUserName" "aUserPwd")
               ;;(pd/request someLockedUrl :request-method :post)
               ;;(pd/request "login-buddy/next?=/" :request-method :post)
               (pd/request "/"
                           :request-method :post
                           :form-params {"username" "aUserName"
                                         "password" "aUserPwd"}
                           :params {:username "aUserName"
                                    :password "aUserPwd"
                                    :next "/"}
                           :query-params {"next" "/"}
                           ;:uri "/login-buddy"
                           :query-string "next=/"
                           :ssl-client-cert nil
                           :protocol "HTTP/1.1"
                           :cookies {"secret" {:value "a2bfb094-55b5-4663-86f3-7564e7187d61"}}
                           :remote-addr "127.0.0.1"
                           ;;:params {:username "nikki", :password "secret", :next "/homedocs"}
                           :flash nil
                           ;; :headers
                           ;; {"origin" "http://localhost:5002"
                           ;;  "host" "localhost:5002"
                           ;;  "user-agent"
                           ;;  "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0"
                           ;;  "content-type" "application/x-www-form-urlencoded"
                           ;;  "cookie" "secret=a2bfb094-55b5-4663-86f3-7564e7187d61"
                           ;;  "content-length" "30"
                           ;;  "referer" "http://localhost:5002/login-buddy?next=/homedocs"
                           ;;  "connection" "keep-alive"
                           ;;  "upgrade-insecure-requests" "1"
                           ;;  "accept"
                           ;;  "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
                           ;;  "accept-language" "en-US,en;q=0.5"
                           ;;  "accept-encoding" "gzip, deflate"}
                           :server-port 5002
                           :content-length 30
                           ;;:form-params {"username" "nikki", "password" "secret"}
                           ;; :session/key nil
                           :query-params {"next" "/"}
                           :content-type "application/x-www-form-urlencoded"
                           :character-encoding nil
                           :uri "/login-buddy"
                           :server-name "localhost"
                           :query-string "next=/"
 ;; :body
 ;; #object[org.eclipse.jetty.server.HttpInputOverHTTP 0x7328df16 "HttpInputOverHTTP@7328df16"],
                           :multipart-params {}
                           :scheme :http
                           :request-method :post
                           :session {}
                           
                           )
               ;(pd/follow-redirect)
               ;; (pd/request "/"
               ;;             :request-method :post
               ;;             :params {:username "aUserName"
               ;;                      :password "aUserPwd"})
               )
           response (:response peridot-response)]
      (println (pformat peridot-response))
      (println (pformat response))
      (is (= 200 (:status response)))
      (is (= securedPageBodyContent
             (get response :body))))))



(defn buddy-login2 []
  (testing "Login"
    (let* [peridot-response ;;(buddy-app (mock/request :get someLockedUrl))
           (-> (pd/session buddy-app)
               (pd/request someLockedUrl)
               (pd/follow-redirect)
               ;;(pd/authorize "aUserName" "aUserPwd")
               ;;(pd/request someLockedUrl :request-method :post)
               ;;(pd/request "login-buddy/next?=/" :request-method :post)
               (pd/request someLockedUrl
                           :uri someLockedUrl
                           :request-method :post
                           :form-params form-params
                           :query-params {"next" "/"}
                           )
               ;;(pd/request someLockedUrl)
               ;(pd/follow-redirect)
               ;;(pd/follow-redirect)
               ;(pd/request someLockedUrl)
               ;(pd/follow-redirect)

               ;; (pd/request "/"
               ;;             :request-method :post
               ;;             :params {:username "aUserName"
               ;;                      :password "aUserPwd"})
               )
           response (:response peridot-response)]
      (println (pformat peridot-response))
      (println (pformat response))
      (is (= 200 (:status response)))
      (is (= securedPageBodyContent
             (get response :body))))))

(defn buddy-login-mock []
  ;;Will try with ring-mock instead of peridot
  (let [get-request (mock/request :get "/")
        post-request (mock/request :post "/login-buddy")]
    (testing "Being redirected?"
      (let [response (buddy-app get-request)]
        (is (= 302 (:status response)))
        (is (= (str "/login-buddy?next=" someLockedUrl)
               (get-in response [:headers "Location"])))))
    (testing "Login failed"
      (let* [req (assoc post-request
                        :form-params {"username" "wrongUser"
                                      "password" "wrongPwd"}
                        :params {:username "agakngakgna"
                                 :password "agklnagandga"
                                 :next someLockedUrl}
                         :query-params {"next" someLockedUrl}
                         :content-type "application/x-www-form-urlencoded"
                         ;:character-encoding nil
                         :uri "/login-buddy"
                         :server-name "localhost"
                         :query-string (str "next=" someLockedUrl)
                        )
             response (buddy-app req)]
        (println (pformat req))
        (println (pformat response))
        (is (= 403 (:status response)))
        (is (= (str "/login-buddy?next=" someLockedUrl)
               (get-in response [:headers "Location"])))))))


(defn buddy-test01 []
  (testing "soemthing"
    (let* [request (-> (mock/request :post someLockedUrl)
                       (mock/json-body dummy-authdata))
           response (buddy-app request)]
      ;;(print response)
      (is (= 0 request))
      (is (= 1 response))
      (is (= 302 (:status response)))
      (is (= (str "/login-buddy?next=" someLockedUrl)
             (get-in response [:headers "Location"]))))
    ))

(deftest buddy-handler-test
  "Testing secure handler"
  (js-hash-fn-available)
  (let* [response (buddy-app (mock/request :get someLockedUrl))]
    ;;(print response)
    (is (= 302 (:status response)))
    (is (= (str "/login-buddy?next=" someLockedUrl)
           (get-in response [:headers "Location"]))))
  ;;(buddy-test01)
  ;;(buddy-login-fail)
  ;(buddy-login-mock) ;;TODO: Fix! Use ring-mock or peridot? 
  ;;(buddy-login)
  ;(buddy-login2)
  )








;;;;;;;;;;;;;;;;; Some requests/responses
;;; When logging in:
;; RECEIVED:
;; {:ssl-client-cert nil,
;;  :protocol "HTTP/1.1",
;;  :cookies {"secret" {:value "a2bfb094-55b5-4663-86f3-7564e7187d61"}},
;;  :remote-addr "127.0.0.1",
;;  :params {:username "nikki", :password "secret", :next "/homedocs"},
;;  :flash nil,
;;  :headers
;;  {"origin" "http://localhost:5002",
;;   "host" "localhost:5002",
;;   "user-agent"
;;   "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0",
;;   "content-type" "application/x-www-form-urlencoded",
;;   "cookie" "secret=a2bfb094-55b5-4663-86f3-7564e7187d61",
;;   "content-length" "30",
;;   "referer" "http://localhost:5002/login-buddy?next=/homedocs",
;;   "connection" "keep-alive",
;;   "upgrade-insecure-requests" "1",
;;   "accept"
;;   "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
;;   "accept-language" "en-US,en;q=0.5",
;;   "accept-encoding" "gzip, deflate"},
;;  :server-port 5002,
;;  :content-length 30,
;;  :form-params {"username" "nikki", "password" "secret"},
;;  :session/key nil,
;;  :query-params {"next" "/homedocs"},
;;  :content-type "application/x-www-form-urlencoded",
;;  :character-encoding nil,
;;  :uri "/login-buddy",
;;  :server-name "localhost",
;;  :query-string "next=/homedocs",
;;  :body
;;  #object[org.eclipse.jetty.server.HttpInputOverHTTP 0x7328df16 "HttpInputOverHTTP@7328df16"],
;;  :multipart-params {},
;;  :scheme :http,
;;  :request-method :post,
;;  :session {}}
;;ANSWERED:
;; {:status 302,
;;  :headers
;;  {"Location" "/homedocs",
;;   "Set-Cookie"
;;   ("ring-session=b5bfa058-0263-4917-933a-16678a5c7e29;Path=/;HttpOnly")},
;;  :body ""}
