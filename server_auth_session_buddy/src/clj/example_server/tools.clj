(ns example-server.tools
  (:require [n-tools-log.init-log4j2] ;; Need to require namespace to init logging before anything else.
            [n-tools-log.log4j2 :as tl]
            ;[environ.core :refer [env]]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [clojure.set :as cset]
            [clojure.tools.logging :as log];;Log
            ;;[clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :as pprint])
  (:gen-class))

;;--------------------------------------------------------
;; String tools
;;--------------------------------------------------------


(defn pformat [& args]
  "Pretty print but *out* is Stringbuffer and not stdout."
  (with-out-str
    (apply pprint/pprint args)))

(defn repeatStr [someStr numtimes]
  (apply str (repeat numtimes someStr)))

;;--------------------------------------------------------
;; DB tools
;;--------------------------------------------------------

(defn table-exists? [db-spec table-name]
  ((comp not empty?)
    ;;This expr should return (if table-name exists) something like:
    ;; ({:table_cat nil, :table_schem "public", :table_name "<table-name>",
    ;; :table_type "TABLE", :remarks nil})
   (jdbc/with-db-metadata [md db-spec]
     (jdbc/metadata-result (.getTables md nil nil table-name nil
                                       ;;(into-array ["TABLE" "VIEW"])
                                       )))))

(defn create-table 
  [& {:keys [db-spec table-name ddl drop-if-exists]
      :or {db-spec false, table-name false, ddl false, drop-if-exists false}
      :as all}]
  "Will create table, but checking if table exists first. If it exists, then
  does nothing. If drop-if-exists is true, then it will drop table (cascade)
  and create it."
  (when (or (false? db-spec) (false? table-name) (false? ddl))
    (throw (Exception. (str "Fn create-table wrong input: " all "\n"
                            "Required arguments: db-spec, table-name and ddl"))))
  (if drop-if-exists
    (let [sql-exprs [(str  "DROP TABLE IF EXISTS " table-name " CASCADE")
                     ddl]]
      (jdbc/db-do-commands db-spec true sql-exprs))
    (if-not (table-exists? db-spec table-name)
      (jdbc/db-do-commands db-spec true [ddl])
      ;;(println (str "Table " table-name " exists already."))
      (log/debug (str "Table " table-name " exists already."))
      )))
