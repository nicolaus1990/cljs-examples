(ns org-html-p5-examples.p5-example1
  (:require [clojure.string :as str]
            [org-html-p5-examples.tools :as t]
            ;;[devtools.core :as devtools]
            ))

(comment "This is an example taken from:"
         "    https://p5js.org/examples/simulate-chain.html"
         "Requirements: "
         "    1. html file to have div with id=p5-example1"
         "    2. loading p5.js before this script."
         "    3. To create example1 p5-sketch, need to call fns: "
         "        - org_html_p5_examples.p5_example1.setup(); from within (global) 'setup' p5 function."
         "        - org_html_p5_examples.p5_example1.draw(); from within (global) 'draw' p5 function.")



(def gravity 9.0)
(def mass 2.0)

;; Springs
(def s1 (atom nil))
(def s2 (atom nil))

(defn get-springs [] [s1 s2])

(defn start-2D-springs []
  (reset! s1 {:m mass :g gravity :radius 30
              :stiffness 0.2 :damping 0.7
              ;;Positions:
              :x 0.0 :y (/ js/width 2)
              ;;Velocities:
              :vx 0 :vy 0})
  (reset! s2 {:m mass :g gravity :radius 30
              :stiffness 0.2 :damping 0.7
              ;;Positions:
              :x 0.0 :y (/ js/width 2)
              ;;Velocities:
              :vx 0 :vy 0}))

(defn update-springs []
  (doseq [[s target-x target-y] [[s1 js/mouseX js/mouseY]
                                 [s2 (:x @s1)  (:y @s1)]]]
    (let* [forceX   (* (- target-x (:x @s)) (:stiffness @s))
           ax       (/ forceX (:m @s))
           forceY   (+ (* (- target-y (:y @s)) (:stiffness @s))
                       (:g @s))
           ay       (/ forceY (:m @s))]
      (swap! s update :vx #(* (:damping @s) (+ % ax)))
      (swap! s update :vy #(* (:damping @s) (+ % ay)))
      (swap! s update :x  + (:vx @s))
      (swap! s update :y  + (:vy @s)))))


(defn display-springs []
  (doseq [[s nx ny] [[s1 js/mouseX js/mouseY]
                     [s2 (:x @s1)  (:y @s1)]]]
    (js/noStroke)
    (js/ellipse (:x @s) (:y @s)
                (* 2 (:radius @s)) (* 2 (:radius @s)))
    (js/stroke 255)
    (js/line (:x @s) (:y @s) nx ny)))

(defn ^:export keyTyped [key keyCode]
  (let [restart? (= "r" keyCode)]
    (when restart?
      (js/fill 255 126)
      (start-2D-springs))))

(defn ^:export setup []
  (let [canvas (js/createCanvas 720 400)]
    (js/fill 255 126)
    (.parent canvas "p5-example1")
    (start-2D-springs)))

(defn ^:export draw []
  (do (js/background 0)
      (update-springs)
      (display-springs)))

(defn run []
  (t/log "--- org-html-p5-examples.p5-example1 ---")
  (t/log "To create example1 p5-sketch, need to call fns: ")
  (t/log "    - org-html-p5-examples.p5-example1.setup(); from within (global) 'setup' p5 function.")
  (t/log "    - org-html-p5-examples.p5-example1.draw(); from within (global) 'draw' p5 function."))


