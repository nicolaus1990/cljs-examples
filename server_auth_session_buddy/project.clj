(defproject example-server "1.0.0-SNAPSHOT"
  :description "My server "
  ;; :license {:name "Eclipse Public License v1.0"
  ;;           :url "http://www.eclipse.org/legal/epl-v10.html"}
  :url ""
  :min-lein-version "2.9.4"

  :main ^:skip-aot example-server.web
  ;; Folder "src/sql/" has to be in source-paths for code to pick up sql
  ;; files (development). In uberjar, it has to be in :resource-paths.
  :source-paths ["src/clj/"
                 "src/cljc/"]
  :test-paths ["src/clj_tests"]
  ;;:java-source-paths ["src/main/java"]
  ;; Non-source-files included in classpath/jar:
  ;;:resource-paths ["resources"]
  :uberjar-name "example-server-standalone.jar"

  ;; Sometimes (for unknown reasons) dependency problems may arise. In such
  ;; cases run:
  ;;     "lein deps :tree > ~/Desktop/deps.txt 2> ~/Desktop/deps-err.txt"
  ;; and see recommendations to exclude conflicting deps.
  ;; Note: Sorting the dependencies array could (ugly-)fix dependency issues, 
  ;; since Maven will use the first dependency it encouters in the tree of
  ;; dependencies.
  :dependencies [[org.clojure/clojure "1.10.1"]
                 ;;--------------------------
                 ;; Clj deps
                 ;;
                 ;;Some hidden dependencies listed first to prevent override
                 [ring/ring-core "1.8.2"]
                 ;;--------------------------
                 ;;Dependencies used:
                 [org.clojure/java.jdbc "0.7.11"] ;;[org.clojure/java.jdbc "0.7.8"]
                 [org.clojure/tools.logging "1.1.0"]
                 [org.clojure/tools.cli "1.0.194"]
                 ;; DB related
                 [org.postgresql/postgresql "42.2.5.jre7"]
                 ;; Web dev
                 [ring/ring-json "0.5.0"]
                 ;;[metosin/muuntaja "0.6.6" :exclusions [commons-codec]]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-jetty-adapter "1.8.1"]
                 [hiccup "1.0.5"]
                 [compojure "1.5.1"]
                 ;; For security:
                 ;;[buddy "2.0.0"]
                 [buddy/buddy-auth "2.1.0"]
                 ;;[buddy/buddy-sign "2.2.0"]
                 ;;Environment variables (needed for heroku)
                 [environ "1.1.0"]
                 ;;-------------------------------------------------------                 
                 ;; Clj log-deps
                 ;; Logging with Log4j2
                 ;;       - log4j-core and log4j-api (for log4j2 itself to work)
                 ;;       - log4j-slf4j-impl implements the SLF4J interface and
                 ;;         forwards the calls to the log4j2 functions. This way
                 ;;         all libraries that use the SLF4J logging interfaces,
                 ;;         automatically log their messages via log4j2.
                 [org.apache.logging.log4j/log4j-core "2.9.1"]
                 [org.apache.logging.log4j/log4j-api "2.9.1"]
                 [org.apache.logging.log4j/log4j-slf4j-impl "2.9.1"]
                 ;;-------------------------------------------------------
                 ;; Cljs deps
                 [org.clojure/clojurescript "1.10.773"]
                 [reagent "1.0.0-alpha2"]
                 ;;-------------------------------------------------------
                 ]
  :plugins [[lein-environ "1.1.0"]
            [lein-ring "0.12.5"]
            [lein-simpleton "1.3.0"]
            ;;To pretty-print a representation of the project map
            [lein-pprint "1.3.2"]
            ;;For deployment
            ;;[lein-heroku "0.5.3"]
            ;;For cljs dev
            ;;Task cljsbuild will be available
            [lein-cljsbuild "1.1.8" :exclusions [[org.clojure/clojure]]]
            ;;For refreshing unit-tests (commented out, because it's in
            ;;  ~/.lein/.profiles.clj 
            ;;[com.jakemccrary/lein-test-refresh "0.24.1"] 
            ]
  ;;When invoking lein clean, remove generated javascript and target folder
  ;;Beware of paths that are removed; lein's protection overridden (see
  ;;metadata) to be able to remove something from "resources/public" folder.
  :clean-targets ^{:protect false} ["resources/public/example_app_todo/js/compiled/"
                                    "resources/public/example_app_another/js/compiled/"
                                    "resources/public/example_app_another/js/compiled_devcards/"
                                    ;;Will remove also target-folder
                                    :target-path]
  
  :cljsbuild {:builds
              [ ;; ------------------------------------------------------------
               ;; Production  builds for apps
               {:id           "min-todo-app"
                :source-paths ["src/cljs/example_app_1"]
                :compiler     {:output-to "resources/public/example_app_1/js/compiled/app.js"
                               :optimizations :advanced
                               :parallel-build true}}
               ;; ------------------------------------------------------------
               ;; Dev cljs build for todo-app
               {:id "todo-app"
                :source-paths ["src/cljs/example_app_1"]
                :compiler {:main todo-app.core
                           :output-to      "resources/public/example_app_1/js/compiled/app.js"
                           :output-dir     "resources/public/example_app_1/js/compiled/out"
                           ;;asset-path = :output-dir minus your webroot directory
                           ;;    where webroot dir = "resources/public"
                           :asset-path     "example_app_1/js/compiled/out"
                           :source-map-timestamp true
                           ;;:preloads             [devtools.preload]
                           :optimizations :none
                           ;;:pseudo-names    false
                           ;;:pretty-print    true
                           }}]}
  ;;For development
  :ring {:handler example-server.web/main}
  :repl-options { ;; If nREPL takes too long to load it may timeout,
                 ;; increase this to wait longer before timing out.
                 ;; Defaults to 30000 (30 seconds)
                 :timeout 120000
                 }
  :profiles { ;;When deploying a jar file, profiles won't be delivered (it is exclusively lein's)
             :dev {:main ^:skip-aot n-tools-log.example-server-dev
                        ;;:main ^:skip-aot n-tools-log.testing-logging
                        ;; You can add src/figwheel_setup/ source path here to get
                        ;; user.clj loaded (Caveat: other projects using n-tools as
                        ;; dep will load it too, which means they will require
                        ;; figwheel).
                        :source-paths ["src/clj" "src/cljc" "src/clj_dev" "src/sql"]
                        ;; Setup target as a resource path for log4j2 xml file.
                        :resource-paths ["resources"]
                        :dependencies  [ ;;------------------------------
                                        ;; Clj dev-deps
                                        ;; Logging with Log4j2
                                        ;;       - log4j-core and log4j-api (for log4j2 itself to work)
                                        ;;       - log4j-slf4j-impl implements the SLF4J interface and
                                        ;;         forwards the calls to the log4j2 functions. This way
                                        ;;         all libraries that use the SLF4J logging interfaces,
                                        ;;         automatically log their messages via log4j2.
                                        ;; [org.apache.logging.log4j/log4j-core "2.9.1"]
                                        ;; [org.apache.logging.log4j/log4j-api "2.9.1"]
                                        ;; [org.apache.logging.log4j/log4j-slf4j-impl "2.9.1"]
                                        ;; ;[org.slf4j/jcl-over-slf4j "1.7.25"];;NOT NEEDED!
                                        ;;------------------------------
                                        ]
                        ;; Heroku will specify port through environment variable.
                        ;; Environment variables are taken with environ lib (heroku example does it this way)
                        :env {:production "false"
                              :port "3001"
                              :log-level "debug"
                              :log-dir "/home/nm/Desktop/tmp/logs/example-server/"
                              ;;:serverUrl "https://nikkis-stuff.herokuapp.com"
                              ;;:path-html-login "public/login/login.html"
                              ;;DB env vars
                              :db-spec-classname "org.postgresql.Driver"
                              :db-spec-subprotocol "postgresql"
                              :db-spec-subname "//localhost:5432/nm_server_test"
                              :db-spec-user "nm_test"
                              :db-spec-password "nm_test"
                              ;;:database-url "jdbc:postgres://localhost:5432/nm_server_test?user=nm_test&password=nm_test"
                              }}
             :cljs-dev [:dev
                        { ;; figwheel: need to add cljs_dev source path here to get user.clj loaded.
                         :source-paths [;; Note: Path "src/cljs" is not enough: Need to specify which app.
                                        "src/cljs/example_app_1"
                                        "src/cljc"
                                        "src/clj"
                                        "src/cljs_dev"]
                         :resource-paths ["target" "resources"]
                         :dependencies [ ;;------------------------------------------------------------
                                        ;; Cljs dev-deps
                                        ;; For dev: wrap-reload is needed (in ring-devel). Useful for
                                        ;; reloading backend (handler) when using in conjuncion with
                                        ;; figwheel.
                                        [ring/ring-devel "1.8.1"]
                                        [com.bhauman/figwheel-main "0.2.15"]
                                        [com.bhauman/rebel-readline-cljs "0.1.4"] ;; optional but recommended
                                        ;;[binaryage/devtools "0.9.10"]
                                        ]}]
             :unit-tests [:dev
                          {:env {:log-level "debug"}
                           :source-paths ["src/clj" "src/cljc" "src/clj_dev"]
                           ;;Note: source-paths should be just: src/clj.
                           :dependencies [[ring/ring-devel "1.6.3"]
                                          [ring/ring-mock "0.4.0"]
                                          ;;For mocking with more interaction
                                          ;;use peridot.
                                          [peridot "0.5.2"]
                                          ]}]
             :production-test {:env {:production "true"
                                     :database_url "jdbc:postgres://nm_test:nm_test@localhost:5432/nm_server_test"
                                     :log-level "debug"
                                     :log-dir "/home/nm/Desktop/tmp/logs/n-cljs-example_server_auth_jwt_buddy/"
                                     :serverUrl "https://nikkis-stuff.herokuapp.com"}}
             :uberjar {:omit-source true
                       :aot :all
                       ;;:resource-paths ["src/sql"]
                       }}
  :aliases {;; Production
            ;; log_level=debug log_dir=/tmp/logs/example-server/ database_url=jdbc:postgres://nm_test:nm_test@localhost:5432/nm_server_test java -Dlog4j.configurationFile=file:/home/nm/Documents/ProgrammingWorkspace/n-app-lists/resources-dev/log4j2.xml -cp target/example-server-standalone.jar clojure.main -m example-server.core start-server --port 3001
            "prod" ["do" "clean," "ball," "p3000"]
            "p3000" ["with-profile" "+production-test"
                     "run" ;;"start-server" "--port"
                     "3000"
                     ;;"--log-level" "debug"
                     ;;"--log-dir" "/tmp/logs/example-server/"
                     ]
            ;;Tests and help
            "ayuda" ["run" "-h"]
            "d3000" ["with-profile" "+dev"
                     "run" "start-server"
                     "--port" "3000"]
            ;; Serves files
            "sv"    ["simpleton" "5000" "file" ":from" "resources"]
            ;; Build with cljsbuild
            "btodo" ["cljsbuild" "once" "min-todo-app"]       ;;Build todo app
            "banother" ["cljsbuild" "once" "min-another-app"] ;;Build another app
            "ball" [ ;;"with-profile" "+cljs-dev"
                    "cljsbuild" "once" "min-todo-app" "min-another-app"] ;;build the cljs once (args after 'once' are ids of apps)
            "batodo" ["with-profile" "+cljs-dev"
                      "cljsbuild" "auto" "todo-app"] ;;watch src for changes and automatically rebuild them
            ;; Figwheel
            ;; remove the "trampoline" option below if you are using Windows.
            "fig"       ["with-profile" "+cljs-dev"
                         "trampoline" "run" "-m" "figwheel.main"]
            ;; (:auto-testing enabled): go to http://localhost:9500/figwheel-extra-main/auto-testing 
            ;; to see results.
            "fig:ex1" ["with-profile" "+cljs-dev"
                       "trampoline" "run" "-m" "figwheel.main" "-b" "example-app-1" "-r"]
            ;; To debug figwheel config --print-config
            "fig:conf" ["with-profile" "+cljs-dev"
                        "trampoline" "run" "-m" "figwheel.main" "-pc" "-b" "example-app-1" "-r"]
            ;;----------------------------------------------------
            ;; Basic lein
            ;; Cleans directories, check, repl, ueberjar
            "C"     ["clean"]
            "c"     ["check"]
            "cp"    ["classpath"]
            "P"     ["show-profiles"]
            "pput"  ["with-profile" "cljs-dev" "pprint"] ;; Print project map
            "r"     ["with-profile" "+dev" "repl"]
            "U"     ["uberjar"]
            "dcljs" ["with-profile" "+cljs-dev" "deps" ":tree"]
            "d"     ["deps" ":tree"] ;; See dependency tree
            ;;(also useful: "deps :tree > ~/Desktop/deps.txt 2> ~/Desktop/deps-err.txt").
            ;;----------------------------------------------------
            ;; Tests
            ;;
            ;; For cljs tests see alias "fig:dev" and "fig:test"
            ;;
            ;; To refresh unit-tests, require plugin: lein-test-refresh "0.24.1"
            "tu" ["with-profile" "+unit-tests" "test"]
            "tur" ["with-profile" "+unit-tests" "test-refresh"]
            "tu0" ["with-profile" "+unit-tests" "test"
                   ":only" "example-server.testing-clojure-stuff"]
            "tu1" ["with-profile" "+unit-tests" "test"
                   ":only" "example-server.routing.desk-api-test/get-browsers-api"]
            "tu2" ["with-profile" "+unit-tests" "test"
                   ":only" "example-server.db.db-desk-test"]
            ;; Server
            "ts" [ ;;"do" "cljsbuild" "once" "todo-app" "another-app,"
                  ;;"do" "cljsbuild" "once" "min-todo-app" "min-another-app,"
                  "with-profile" "+cljs-dev" "run"]
            ;;----------------------------------------------------
            })
