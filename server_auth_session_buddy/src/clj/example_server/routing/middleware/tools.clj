(ns example-server.routing.middleware.tools
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response redirect]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [example-server.tools :refer [pformat]]
            ;;Logging:
            [clojure.tools.logging :as log]))

(def ^:dynamic *log?* true #_false)

;; TODO: To n-tools
(defn log-middleware-only-request
  ;;Middleware to debug request
  ([handler] (log-middleware-only-request handler "RECEIVED: "))
  ([handler prefixReq]
   (fn [request]
     (when *log?* 
       (log/debug (str prefixReq (pformat request))))
     (handler request))))

(defn log-middleware
  ;;Middleware to debug request and check response
  ([handler] (log-middleware handler "RECEIVED: " "ANSWERED: "))
  ([handler prefixReq prefixRes]
   (fn [request]
     (let [response (handler request)]
       (when *log?* 
         (log/debug (str prefixReq (pformat request)))
         (log/debug (str prefixRes (pformat response))))
       response))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Convert clean url in request to dirty url
;;; (for compojure.route.resource to handle)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn dirty-url? [url]
  (let [index-of-slash (or (s/last-index-of url "/") -1)
        index-of-point (or (s/last-index-of url ".") -1)]
    (< index-of-slash index-of-point)))

(defn clean-url->dirty-url [url]
  ;; Clean urls don't have an extension (".html",...) nor "index.html".
  (let [index-of-slash (or (s/last-index-of url "/") -1)
        index-of-point (or (s/last-index-of url ".") -1)
        len (count url)]
    ;;If (last) slash before (last) point, then extension was given (url dirty).
    ;;Else, if it's a uri-subcategory (like a folder) return index.html file.
    ;;Else, it's supposed to be an html file (url was clean).
    (cond (< index-of-slash index-of-point)    url
          (= len (inc index-of-slash))         (str url "index.html")
          (= -1 index-of-point)                (str url "/index.html")
          :else                                (do
                                                 (log/warn (str "Unexpected url:"
                                                                 url))
                                                 url)
          )))

(defn clean-url->dirty-url-middleware [handler]
  ;; Makes out of cleaned urls dirty ones. 
  (fn [req]
    (when *log?*
      (log/debug "Using middleware: clean-url->dirty-url-middleware")
      (log/debug (str "      Previous uri: " (:uri req)))
      (log/debug (str "      New uri: " (:uri (update req :uri
                                                      clean-url->dirty-url)))))
    (-> req
        (update :uri clean-url->dirty-url)
        (handler))))







