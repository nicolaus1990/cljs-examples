(ns apps.things.view
  (:require [clojure.string :as s]
            [clojure.java.io :as io]
            [hiccup.core :as hi]
            [hiccup.form :as hif]
            [hiccup.element :as hie]
            ;;DB
            [apps.things.db :as db]
            ;;Logging:
            [clojure.tools.logging :as log]))

(def app-title "My public list")


;; (defn splash []
;;   {:status 200
;;    :headers {"Content-Type" "text/plain"}
;;    :body "Hello World!"})

(def quick-form 
  (hi/html
   (hi/html [:span {:class "someClassName"} [:p  app-title]])
   (hif/form-to {:enctype "multipart/form-data"}
    [:post "/things"]
   (hif/text-field "Store")
   (hif/submit-button {:class "btn" :name "submit"} "Save")
   (hif/submit-button {:class "btn" :name "submit"} "Remove all"))))



(defn splash [db-spec]
  {:status 200
   :headers {"Content-Type" "text/html"}
   ;;:head (concat "<title>" app-title "</title>") ;;Does not seem to work (probably overridden later)
   :body (concat quick-form
                 ["<hr /><ul>"]
                 (db/get-sayings db-spec #(format "<li>%s</li>" (:content %)))
                 ["</ul>"])})
