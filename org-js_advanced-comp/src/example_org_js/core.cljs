(ns ^:figwheel-hooks example-org-js.core
  (:require
   [goog.dom :as dom]
   [goog.dom.TagName :as TagName]
   ;;[goog.events :as evts]
   [example-org-js.readtheorg-fns :as rto]
   [example-org-js.org-js-fns :as oj]
   [example-org-js.tools :as t]
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom]))

(println "This text is printed from src/n_org_editor/core.cljs. Go ahead and edit it and see reloading in action.")


(defn get-app-element []
  (dom/getElement "app"))

(defn empty-div [] [:div])

(defn mount-reagent-app [el]
  (rdom/render [empty-div] el))


(defn mount []
  (when-let [el (get-app-element)]
    (let [{:keys [div-result] :as divs} (oj/insert-org-editor-html-nodes el)]
      (mount-reagent-app div-result)
      (oj/set-org! div-result oj/example-org-html-doc)
      (oj/log-org-html-doc oj/example-org-html-doc)
      (rto/adapt-html-for-readtheorg-css divs))))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount)

;; specify reload hook with ^:after-load metadata
(defn ^:after-load on-reload []
  (mount)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
