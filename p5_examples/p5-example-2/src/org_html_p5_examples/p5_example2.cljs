(ns ^:figwheel-hooks org-html-p5-examples.p5-example2
  (:require [clojure.string :as str]
            [org-html-p5-examples.tools :as t]
            ;; Foreign-lib p5.js:
            ;;[processing.p5]
            [org-html-p5-examples.p5-cljs-tools :as p5t]
            ;;[devtools.core :as devtools]
            ))

(comment "This is an example taken from:"
         "    https://p5js.org/examples/simulate-chain.html"
         "Requirements: "
         "    1. html file to have div with id=p5-example2"
         "    2. loading p5.js before this script."
         "    3. To create example1 p5-sketch, need to dynamically set fns: "
         "        - org_html_p5_examples.p5_example1.setup(); to be 'setup' p5 function (of p5 instance)."
         "        - org_html_p5_examples.p5_example1.draw();  to be 'draw'  p5 function (of p5 instance).")



(def gravity 9.0)
(def mass 2.0)

;; Springs
(defonce s1 (atom nil))
(defonce s2 (atom nil))

(defn get-springs [] [s1 s2])

(defn start-2D-springs []
  (reset! s1 {:m mass :g gravity :radius 30
              :stiffness 0.2 :damping 0.7
              ;;Positions:
              :x 0.0 :y (/ (p5t/get-width) 2)
              ;;Velocities:
              :vx 0 :vy 0})
  (reset! s2 {:m mass :g gravity :radius 30
              :stiffness 0.2 :damping 0.7
              ;;Positions:
              :x 0.0 :y (/ (p5t/get-width) 2)
              ;;Velocities:
              :vx 0 :vy 0}))

(defn update-springs []
  (doseq [[s target-x target-y] [[s1 (p5t/get-mouseX) (p5t/get-mouseY)]
                                 [s2 (:x @s1)         (:y @s1)]]]
    (let* [forceX   (* (- target-x (:x @s)) (:stiffness @s))
           ax       (/ forceX (:m @s))
           forceY   (+ (* (- target-y (:y @s)) (:stiffness @s))
                       (:g @s))
           ay       (/ forceY (:m @s))]
      (swap! s update :vx #(* (:damping @s) (+ % ax)))
      (swap! s update :vy #(* (:damping @s) (+ % ay)))
      (swap! s update :x  + (:vx @s))
      (swap! s update :y  + (:vy @s)))))


(defn display-springs []
  (doseq [[s nx ny] [[s1 (p5t/get-mouseX) (p5t/get-mouseY)]
                     [s2 (:x @s1)         (:y @s1)]]]
    (p5t/no-stroke)
    (p5t/ellipse (:x @s) (:y @s)
                (* 2 (:radius @s)) (* 2 (:radius @s)))
    (p5t/stroke 255)
    (p5t/line (:x @s) (:y @s) nx ny)))

(defn ^:export keyTyped [key keyCode]
  ;; TODO: Modify create-sketch fn to support p5.js keyTyped
  (let [restart? (= "r" keyCode)]
    (when restart?
      (p5t/fill 255 126)
      (start-2D-springs))))

(defn ^:export setup []
  (let [canvas (p5t/create-canvas 720 400)]
    (p5t/fill 255 126)
    (.parent canvas "p5-example2")
    (start-2D-springs)))

(defn ^:export draw []
  (do (p5t/background 0)
      (update-springs)
      (display-springs)))


;; Define your app data so that it doesn't get over-written on reload.
;; We need to store the sketch for figwheel (hot-reloading), since we 
;; need to stop drawing (no-loop), and for that we need its p5 obj 
;; context. Else if we do not store the p5 sketch in a variable, we 
;; loose access to it, even though it will still run the draw function 
;; undefinitely (thanks to fn 'p5t/with-sketch' saving its p5 context).
(defonce this-sketch (atom nil))

(defn run []
  (t/log "--- org-html-p5-examples.p5-example2 ---")
  (t/log "Note: To create example2 p5-sketch, will dynamically set: ")
  (t/log "    - org-html-p5-examples.p5-example2.setup(); to be 'setup' p5 function (of p5 instance).")
  (t/log "    - org-html-p5-examples.p5-example2.draw();  to be 'draw'  p5 function (of p5 instance).")
  ;; Note: This will not work:
  ;;(set! js/setup org-html-p5-examples.p5-example2/setup)
  ;;(set! js/draw  org-html-p5-examples.p5-example2/draw)
  ;; This works (p5 instant creation):
  (let [sketch (p5t/create-sketch {:setup setup
                                   :draw draw
                                   :element-id "p5-example2"})]
       (reset! this-sketch sketch)))

;;----------------------------------------------------
;; Figwheel reload hooks
;; first notify figwheel that this ns has callbacks defined in it
;; with: (ns ^:figwheel-hooks ...)
;; See: https://figwheel.org/docs/hot_reloading.html
;;---------------------------------------------------

(defn ^:before-load my-before-reload-callback []
  (t/log "You can actually have :figwheel-hooks individually in each file. Here "
         "this function has to call the before-load fns of each namespaces.")
  (t/log "Before reload...")
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  (p5t/no-loop @this-sketch)
  (t/log "s1 := " (str @s1))
  (t/log "s2 := " (str @s2)))

(defn ^:after-load my-after-reload-callback []
  (t/log "After reload...")
  ;; Note: Can't use function 'start-2D-springs' because function is run in
  ;; context of a p5's setup or draw function (it is using fn p5t/get-width).
  ;; Therefore we only partially update state:
  ;;(start-2D-springs) ;Will not work
  (swap! s1 merge {:m mass :g gravity})
  (swap! s2 merge {:m mass :g gravity})
  (t/log "s1 after := " (str @s1))
  (t/log "s2 after := " (str @s2))
  (p5t/yes-loop @this-sketch))
