(ns example-org-js.readtheorg-fns
  (:require [goog.dom :as dom]
            [goog.dom.TagName :as TagName]
            [example-org-js.tools :as t]))


(defn adapt-html-for-readtheorg-css [{:keys [div-result] :as divs}]
  ;; (i)  See also fn insert-org-editor-html-nodes, there a node is added with
  ;;      id of readtheorg (and not the original id of org-js).
  (t/log "Previous div with id 'result' (org-js) has now id 'content' (readtheorg).")
  ;; (ii) Parser org-js puts table-of-contents in a 'ul' list inside 'result'
  ;;      div. Need to create new div node with id "table-of-contents" and put
  ;;      the 'ul' node inside.
  (let [ul-node (-> (dom/getElementsByTagName TagName/UL div-result)
                 array-seq
                 first)]
    (if ul-node
      (.setAttribute ul-node "id" "table-of-contents")
      (t/log "No ul-node found (case ii)."))))
