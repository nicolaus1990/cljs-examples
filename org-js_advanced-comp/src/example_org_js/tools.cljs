(ns example-org-js.tools)

(defn log [& txts] (.log js/console (apply str txts)))
(defn log-js-obj [obj] (.stringify js/JSON obj nil 1))
