(ns example-server.testing-clojure-stuff
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [example-server.env-vars :refer [my-env]]))

;;;;;;; Preliminary

;;(def db "postgres://localhost:5432/test_some_db")
(def db
  ;; Should be:
  ;; {:classname "org.postgresql.Driver"
  ;;  :subprotocol "postgresql"
  ;;  :subname "//localhost:5432/nm_server_test"
  ;;  :user "nm"
  ;;  :password "secret"}
  (:db-spec my-env))

;;;;; Fixtures

;;; Fixtures for keeping database consistent

(declare ^:dynamic *txn*)

(defn always-rollback [f]
  (do ;;(println "setup each db-rollback")
      (jdbc/with-db-transaction
        ;; Symbol db refers to 'db' defined above.
        [transaction db]
        ;;From doc of 'db-set-rollback-only!': "Marks the outermost transaction
        ;;such that it will rollback rather than commit when complete."
        (jdbc/db-set-rollback-only! transaction)
        (binding [*txn* transaction] (f)))
      ;;(println "teardown each db-rollback")
      ))

(defn once-fixture [f]
  (println "setup once")
  (f)
  (println "teardown once"))
(defn each-fixture [f]
  (println "setup each")
  (f)
  (println "teardown each"))
(use-fixtures :each always-rollback)
;;(use-fixtures :each each-fixture)
;;(use-fixtures :once once-fixture)


;;;;;;;;; Tests

;;;; Set-up test database:

(def test-table "some_table_testing_clj_stuff")
(def test-table-kw (keyword test-table));; should be :some_table_testing_clj_stuff

(defn db-create-table [db-spec]
  "Note: Create table command can be rolled back with postgres but not for
  example with mysql (DDL causes an implicit commit)."
  (jdbc/db-do-commands db-spec false
                       [(str "DROP TABLE IF EXISTS " test-table)
                        (jdbc/create-table-ddl test-table-kw
                                               [[:symbol "VARCHAR(10)"]])]))

(defn db-insert-dummy-data [db-spec]
  (jdbc/insert-multi! db-spec test-table-kw
                      [{:symbol "A"}
                       {:symbol "BB"}
                       {:symbol "CCC"}]))

(defn setup-test-db [db-spec]
  (db-create-table db-spec)
  (db-insert-dummy-data db-spec))

;;(db-create-table db)

;;; Assuming these were the functions to test:
(defn get-symbols [db-spec]
  (map :symbol (jdbc/query db-spec [(str "select * from " test-table)])))
(defn insert-symbol! [db-spec sym]
  (jdbc/insert! db-spec test-table-kw {:symbol sym}))


;;;; Testing database with fixtures

(deftest db-ddl
  (testing "SQL Expressions"
    (is (= "CREATE TABLE some_table_testing_clj_stuff (symbol VARCHAR(10))"
           (jdbc/create-table-ddl test-table-kw
                                  [[:symbol "VARCHAR(10)"]])))))

(deftest db-retrieve-some-data
  (testing "Retrieve some dummy data"
    (setup-test-db *txn*)
    (is (some #{"A"} (get-symbols *txn*)))
    (is (some #{"BB"} (get-symbols *txn*)))))

(deftest db-insert-new-data
  (setup-test-db *txn*)
  (testing "Inserting new sym"
    (insert-symbol! *txn* "Z")
    (is (some #{"Z"} (get-symbols *txn*)))))

(deftest db-inserted-new-data-is-rolled-backed
  (setup-test-db *txn*)
  (testing "Inserted new sym is rolled backed"
    (is (not (some #{"Z"} (get-symbols *txn*))))))



;;;;;;;;;;;; More tests

(defn conj-t []
  (testing "conj"
    (are [expected result]
        (= expected result)
      ["a" "b"] (conj ["a"] "b")
      {:a "aa"} (conj {} {:a "aa"})
      {:a "aa" :b "bb"} (conj {:a "aa"} {:b "bb"}))))

(defn booleans-t []
  (testing "What is nil?"
    (is (not (= nil true)))
    (is (not (= nil false)))))

(deftest sandbox
  "Sandbox: Check how clojure evals stuff"
  #_(is (= 0 1))
  (conj-t)
  (booleans-t))
