(ns example-server.routing.routing-test
  (:require [clojure.test :refer :all]
            [environ.core :refer [env]]
            [compojure.core :as cc]
            [compojure.route :as route]))


(defn example-test []
  (testing "Another test"
    (is (= 1 (- 2 1)))))

(deftest some-test
  (testing "Bundled tests"
      (example-test)))
