// [[file:../index.org::*Wrapper of cljs code (P5.js)][Wrapper of cljs code (P5.js):1]]
function setup() {
  console.log("P5 - SETUP function");
  org_html_p5_examples.p5_example1.setup();
}

function draw() {
    org_html_p5_examples.p5_example1.draw();
}

function keyPressed() {
    if (keyCode !== 91) {
	org_html_p5_examples.p5_example1.keyTyped(key, keyCode);
    }
}
// Wrapper of cljs code (P5.js):1 ends here
