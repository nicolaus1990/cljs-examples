(ns apps.things.routes
  (:require [clojure.string :as s]
            [clojure.java.io :as io] 
            ;;Web-dev
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response redirect]]
            [hiccup.core :as hi]
            [hiccup.form :as hif]
            [hiccup.element :as hie]
            [apps.things.view :as view]
            ;;DB
            [apps.things.db :as db]
            [example-server.db :as server-db]
            ;;Logging:
            [clojure.tools.logging :as log]))

(defroutes webpages-list
  (GET "/things" []
       (view/splash (server-db/get-db-spec))
       ;;TODO: file-response not working...
       ;;(file-response "index.html" {:root "lists"})
       )
  (POST "/things" [:as request]
        ;;curl -F Store='Hello from CURL' -F submit=Save http://localhost:5000/things
        (let [s (request :multipart-params)]
          (log/debug (str "Multipart-params of form: " s))
          ;; Note: It would be better to give each button a unique name and
          ;; check that (see:
          ;; https://stackoverflow.com/questions/547821/two-submit-buttons-in-one-form).
          (case (get-in s ["submit"])
            "Save"
            (db/set-saying (server-db/get-db-spec) (get-in s ["Store"]))
            "Remove all"
            (db/reset-saying (server-db/get-db-spec))
            ;(log/debug "Not implemented yet.")
            ;Default:
            (log/error "Shouldn't have happened!"))
          (redirect "/things"))))



