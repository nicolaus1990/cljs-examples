(ns example-server.env-vars-test
  (:require [clojure.test :refer :all]
            [example-server.env-vars :refer [establish-port my-env]]
            [environ.core :refer [env]]))

(comment
"This test presupposes the following specificiation:"
(defproject example-server "1.0.0-whateva"
  [...]
  :dependencies [[environ "1.1.0"] [...]]
  :profiles {:local-test {:dependencies [...]
                          :plugins [...]
                          :env
                          {:log-level "debug"
                           :port "5002"
                           :path-html-login "public/login/login.html"
                           ;;DB env vars
                           :db-spec-classname "org.postgresql.Driver"
                           :db-spec-subprotocol "postgresql"
                           :db-spec-subname "//localhost:5432/nm_server_test"
                           :db-spec-user "nm_test"
                           :db-spec-password "nm_test"
                           ;;:database-url "jdbc:postgres://localhost:5432/nm_server_test?user=nm_test&password=nm_test"
                           }}}))

(defn test-db-specs []
  (testing "Also db specs."
    (is (= {:classname "org.postgresql.Driver"
            :subprotocol "postgresql"
            :subname "//localhost:5432/nm_server_test"
            :user "nm_test"
            :password "nm_test"}
           (:db-spec my-env)))
    (is (not (= {:classname "whateva"}
                (:db-spec my-env))))))


(deftest test-env-test-vars
  (testing "Testing if environment vars are caught (note: environ cast vals to string)"
    (are [expected actual] (= expected actual)
      nil (System/getenv "log-level")
      ;;Note: environ casts vals to strings
      "debug" (env :log-level)
      "5002" (env :port))
    (testing "and if they are parsed correctly."
      (are [expected actual] (= expected actual)
        :debug (:log-level my-env)
        true (:log-write-to-file my-env)
        nil (:port my-env)
        5002 (establish-port nil)
        5011 (establish-port 5011))
      (test-db-specs))))
