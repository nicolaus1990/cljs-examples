(defproject n-cljs-examples "0.1.0-SNAPSHOT"
  :description "Sandbox to test everything clojurescript (external libraries,
  figwheel-main, and other tools."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.773"]
                 [reagent "0.10.0" ]
                 [binaryage/oops "0.7.1"]
                 [cljsjs/react-sortable-hoc "1.11.0-1"]]

  :source-paths ["src"]

  :aliases {"fig"       ["run" "-m" "figwheel.main"]
            "fig:build" ["run" "-m" "figwheel.main" "-b" "dev" "-r"]
            "fig:min"   ["run" "-m" "figwheel.main" "-O" "advanced" "-bo" "dev"]
            "fig:test"  ["run" "-m" "figwheel.main" "-co" "test.cljs.edn" "-m" "n-cljs-examples.test-runner"]
            "fig:ctest" ["run" "-m" "figwheel.main"
                         "-co" "test_cline.cljs.edn"
                         "-m" "n-cljs-examples.test-runner-only-command-line"]
            "fig:prod"  ["run" "-m" "figwheel.main" "-O" "advanced" "-bo" "prod"]}

  :profiles {:dev {:dependencies [[com.bhauman/figwheel-main "0.2.15"]]
                   
                   :resource-paths ["target"]
                   ;; need to add the compiled assets to the :clean-targets
                   :clean-targets ^{:protect false} ["target"]}})

