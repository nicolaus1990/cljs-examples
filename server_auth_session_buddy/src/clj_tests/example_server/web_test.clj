(ns example-server.web-test
  ;;To run only this test file: lein test :only example-server.web-test
  (:require [clojure.test :refer :all]
            [clojure.string :as s]
            [environ.core :refer [env]]
            [ring.mock.request :as mock]
            [example-server.db :as dba]
            [example-server.web :as nw]
            [example-server.test-db-setup :refer [*txn* always-rollback]]
            ))

(use-fixtures :each always-rollback)

(defn setup-admin-table [db]
  (dba/create-table-users-if-non-existant db)
  #_(insert-dummy-data db))



(defn homedocs-test []
  (testing "Testing main route: homedocs"
    (let [response ((nw/make-app) (mock/request :get "/homedocs"))
          urlSubstringResponse "/login-buddy?next=/homedocs"]
      (is (= 302 (:status response)))
      (is (s/includes? (get-in response [:headers "Location"])
                       urlSubstringResponse)))))

(deftest app-test
  "Testing main route"
  (setup-admin-table *txn*)
  ;;Test some status codes
  (are [expectedStatus someURL]
      (= expectedStatus (:status ((nw/make-app) (mock/request :get someURL))))
    200 "/pages/"
    200 "/things"
    302 "/homedocs"
    404 "Whateva")
  (homedocs-test)
  )
