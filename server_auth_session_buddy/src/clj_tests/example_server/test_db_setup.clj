(ns example-server.test-db-setup
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [example-server.env-vars :refer [my-env]]))



;;--------------------------------------------------
;;-------- Set-up test database:
;;--------------------------------------------------



(def db
  ;; Should be:
  ;; {:classname "org.postgresql.Driver"
  ;;  :subprotocol "postgresql"
  ;;  :subname "//localhost:5432/nikkis_server_test"
  ;;  :user "nikki"
  ;;  :password "secret"}
  (:db-spec my-env))

;;; This is going to be the variable for db-transaction object
(declare ^:dynamic *txn*)


(defn always-rollback [f]
  (do ;;(println "setup each")
      (jdbc/with-db-transaction
        [transaction db]
        ;;From doc of 'db-set-rollback-only!': "Marks the outermost transaction
        ;;such that it will rollback rather than commit when complete."
        (jdbc/db-set-rollback-only! transaction)
        (binding [*txn* transaction] (f)))
      ;;(println "teardown each")
      ))

;;--------------------------------------------------
;;--------------- Fixtures examples
;;--------------------------------------------------

#_(defn each-fixture [f]
  (println "setup each")
  (f)
  (println "teardown each"))

#_(use-fixtures :each ;;each-fixture
  always-rollback)

#_(defn once-fixture [f]
  (println "setup once")
  (f)
  (println "teardown once"))
#_(use-fixtures :once once-fixture)


;;--------------------------------------------------
;;--------------- Example to how to use this module
;;--------------------------------------------------

(comment

  (ns example-server.test-db-example
    (:require [clojure.test :refer :all]
              [clojure.java.jdbc :as jdbc]
              [example-server.some-module.to-test :as dba]
              [example-server.test-db-setup :refer [*txn* always-rollback]]
              [example-server.env-vars :refer [my-env]]))
  ;; ---- Setup db
  (use-fixtures :each ;;each-fixture
    always-rollback)
  (def data {:username "nikki" :password "secret"})
  (defn insert-dummy-data [db]
    (jdbc/insert! db :my_users data))
  (defn setup-db [db]
    (dba/create-table-users-if-non-existant db)
    (insert-dummy-data db))

  ;; ---- Tests
  (deftest check-authdata
    (setup-db *txn*)
    (testing "Checking data"
      (is (= data (dba/get-data *txn*)))))
  )
