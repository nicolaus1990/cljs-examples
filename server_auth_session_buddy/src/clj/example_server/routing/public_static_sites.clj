(ns example-server.routing.public-static-sites
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.route :as route]
            [ring.util.response :refer [file-response resource-response]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.util.response :refer [redirect]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            ;;[hiccup.middleware :refer [wrap-base-url]]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [example-server.routing.middleware.tools :as mid]
            ;;Environment variables
            [example-server.env-vars :refer [my-env]]
            #_[ring.middleware.reload :refer [wrap-reload]];; Note: only available in dev deps 
            ;;Logging:
            [clojure.tools.logging :as log]))

(def static-files
  (-> (route/resources "/")
      (wrap-content-type)
      (wrap-not-modified)
      (mid/clean-url->dirty-url-middleware)))

;; (def static-files-dev 
;;   ;; Development handler: wrap-reload reloads ns of modified files before the
;;   ;; request is passed to handler.
;;   (wrap-reload #'static-files {:dirs ["src/clj"]}))

