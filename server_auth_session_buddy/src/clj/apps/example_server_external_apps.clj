(ns apps.example-server-external-apps
  (:require [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            ;;BEGIN_REQUIRE_EXTERNAL_APPS
            [apps.n-app-lists-server.core :as APP-NALS]
            [apps.things.routes           :as APP-THINGS]
            ;;END_REQUIRE_EXTERNAL_APPS
            ;;DB
            [example-server.db :as db]))


(defn build-handlers-apis-secured []
  (routes
   ;;BEGIN_EXTERNAL_APPS Apps (non-static ones)
   (APP-NALS/make-handler-prod (db/get-db-spec))
   ;;END_EXTERNAL_APPS
   ))

(defn build-handlers-apis []
  (routes
   ;;BEGIN_EXTERNAL_APPS Apps (non-static ones)
   APP-THINGS/webpages-list
   ;;END_EXTERNAL_APPS
   ))
