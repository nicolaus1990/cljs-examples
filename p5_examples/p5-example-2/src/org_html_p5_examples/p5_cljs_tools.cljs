(ns org-html-p5-examples.p5-cljs-tools
  (:require [clojure.string :as str]
            ;;[cljsjs.p5]
            ;; Foreign-lib p5.js:
            [processing.p5]
            [org-html-p5-examples.tools :as t]
            [oops.core :refer [oget oset! ocall oapply ocall! oapply!
                               oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]
            ))

(comment (str "This code was taken from: https://github.com/pablinos/p5-cljs"
              "Precisely: https://github.com/pablinos/p5-cljs/blob/master/src/p5_cljs/core.cljs"))

(def ^:dynamic *current-sketch* nil)

(defn get-sketch [] *current-sketch*)

(defn with-sketch [obj func]
  (fn []
    (this-as this
      (binding [*current-sketch* obj]
        ;; (t/log "WARNING: Uncomment these logs in production "
        ;;        "(since they might run too many times)")
        ;; (t/log "NOTE: *current-sketch* should not be null (or undefined)")
        ;; (t/log "*current-sketch* := " *current-sketch*)
        (func))))
)

(defn apply-to-sketch [fun & args]
  ;; Without needing advanced compilation:
  #_(let [sketch (get-sketch)
        f (aget sketch fun)]
    (.apply f sketch (clj->js args))
    )
  ;; To make advance compilation succeed:
  (let [sketch (get-sketch)
        f (oget+ sketch fun)
        ;;f-apply (oget+ f "apply")
        ]
    (ocall!+ f "apply" sketch (clj->js args))
    ;; These did not work:
    ;;(ocall!+ f "apply" sketch args)
    ;;(ocall f-apply f sketch (clj->js args))
    ;;(f-apply f sketch (clj->js args))
    ;;(oapply! fun sketch (clj->js args))
    ;;(oapply! f sketch args)
    ;;(ocall f-apply args)
    ;;(ocall f "apply" args)
    ;;(oapply fun (get-sketch) args)
    ;;(oapply (get-sketch) fun args)
    ))


(defn resolve-constant [key]
  (aget js/p5.prototype (str/upper-case (name key)))
)

(defn frame-rate [num]
  #_(.frameRate (get-sketch) num)
  (ocall!+ (get-sketch) "frameRate" num)
)

(defn pixel-density []
  #_(.pixelDensity (get-sketch))
  (ocall!+ (get-sketch) "pixelDensity")
)

(defn push-matrix []
  #_(.push (get-sketch))
  (ocall!+ (get-sketch) "push")
)

(defn pop-matrix []
  #_(.pop (get-sketch))
  (ocall!+ (get-sketch) "pop")
)

(defn millis []
  #_(.millis (get-sketch))
  (ocall!+ (get-sketch) "millis")
)

(defn no-fill []
  #_(.noFill (get-sketch))
  (ocall!+ (get-sketch) "noFill")
)

(defn no-stroke []
  #_(.noStroke (get-sketch))
  (ocall!+ (get-sketch) "noStroke")
)

(defn clear []
  #_(.clear (get-sketch))
  (ocall!+ (get-sketch) "clear")
)

(defn stroke [& args]
  (apply-to-sketch "stroke" args)
)

(defn fill [& args]
  (apply-to-sketch "fill" args)
)

(defn background [& args]
  (apply-to-sketch "background" args)
)

(defn translate [x y]
  #_(.translate (get-sketch) x y)
  (ocall!+ (get-sketch) "translate" x y)
)

(defn rotate [angle]
  #_(.rotate (get-sketch) angle)
  (ocall!+ (get-sketch) "rotate" angle)
)

(defn create-canvas [width height]
  ;; Without needing advanced compilation:
  #_(.createCanvas (get-sketch) width height)
  ;; To make advance compilation succeed:
  (ocall!+ (get-sketch) "createCanvas" width height)
)

(defn rect-mode [mode]
  #_(.rectMode (get-sketch) (resolve-constant mode))
  (ocall!+ (get-sketch) "rectMode" (resolve-constant mode))
)

(defn color-mode [mode]
  #_(.colorMode (get-sketch) (resolve-constant mode))
  (ocall!+ (get-sketch) "colorMode" (resolve-constant mode))
)

(defn create-sketch [options]
  (let [p  (js/p5. (fn [obj]
                     (aset obj "setup" (with-sketch obj (:setup options)))
                     (aset obj "draw"  (with-sketch obj (:draw options))))
                   (:element-id options))]
    p))

(defn no-loop
  ([]       (no-loop (get-sketch)))
  ([sketch] (ocall!+ sketch "noLoop")))

(defn yes-loop
  ([]       (yes-loop (get-sketch)))
  ([sketch] (ocall!+ sketch "loop")))

(defn curve-vertex [x y] 
  #_(.curveVertex (get-sketch) x y)
  (ocall!+ (get-sketch) "curveVertex" x y))

(defn stroke-weight [weight]
  #_(.strokeWeight (get-sketch) weight)
  (ocall!+ (get-sketch) "strokeWeight" weight))

(defn rect [x y w h]
  #_(.rect (get-sketch) x y w h)
  (ocall!+ (get-sketch) "rect" x y w h))

(defn ellipse [x y w h] 
  #_(.ellipse (get-sketch) x y w h)
  (ocall!+ (get-sketch) "ellipse" x y w h))

(defn line [x y nx ny]
  #_(.line (get-sketch) x y nx ny)
  (ocall!+ (get-sketch) "line" x y nx ny))

#_(defn get-width  [] (.-width  (get-sketch)))
#_(defn get-mouseX [] (.-mouseX (get-sketch)))
#_(defn get-mouseY [] (.-mouseY (get-sketch)))

(defn get-width  [] (oget+ (get-sketch) "width"))
(defn get-mouseX [] (oget+ (get-sketch) "mouseX"))
(defn get-mouseY [] (oget+ (get-sketch) "mouseY"))
