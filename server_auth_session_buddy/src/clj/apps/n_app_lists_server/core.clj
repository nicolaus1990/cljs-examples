(ns apps.n-app-lists-server.core
  (:require ;;These ring deps came (probably?) with figwheel.
            [ring.util.response :refer [response status resource-response content-type not-found]]
            [ring.adapter.jetty :as jetty]
            #_[ring.middleware.reload :refer [wrap-reload]]
            [apps.n-app-lists-server.lists-api :refer [api-lists]]
            [apps.n-app-lists-server.db :refer [tn-list db-init db-spec]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [compojure.route :as route]
            [compojure.core :refer [defroutes context routes]]
            [clojure.tools.logging :as log]
            ;; For heroku environment vars
            [environ.core :refer [env]])
  ;; Needs :gen-class, because when building uberjar it warns:
  ;; "Warning: The Main-Class specified does not exist within the jar. It may
  ;; not be executable as expected. A gen-class directive may be missing in
  ;; the namespace which contains the main method, or the namespace has not
  ;; been AOT-compiled."
  ;; And in fact, in will not find clojure.main when running jar like this:
  ;; $ java -cp target/n-app-lists-0.1.0.jar clojure.main -m n-app-lists-server.core
  (:gen-class))

#_(comment

    "This server will need following dependencies in lein's project map:" 

    [;;...
     ;;-------------------------------------------------------
     ;; App: n-app-lists dependencies
     [org.clojure/clojure "1.10.0"]
     ;; For DB-API
     [org.clojure/java.jdbc "0.7.8"]
     [org.postgresql/postgresql "42.2.5.jre7"]
     [ring/ring-json "0.5.0"]
     [ring/ring-defaults "0.3.2"]
     [ring/ring-jetty-adapter "1.4.0"]
     [compojure "1.5.1"]
     ;;-------------------------------------------------------
     ;;...
     ]

    )

(defn wrap-static-files [handler & {:keys [resource-path]
                                    :or {resource-path "public"}}]
  "Middleware: 3 middlewares: wrap-resource, wrap-content-type and
  wrap-not-modified."
  (-> handler
      (wrap-resource resource-path)
      (wrap-content-type)
      (wrap-not-modified)))




;; the routes that we want to be resolved to index.html
(def route-set #{"/" tn-list})

(defn static-file-index [req]
  ;; Taken from: https://figwheel.org/docs/ring-handler.html
  (or
   (when (route-set (:uri req))
     (some-> (resource-response "index.html" {:root "public"})
             (content-type "text/html; charset=utf-8")))
   (not-found "Not found")))


(def build-handlers-dev (routes api-lists
                                (wrap-static-files static-file-index
                                                      :resource-path "public")))


(defn make-app-dev []
  (do (log/info "Setting up database tables (if non-existant)...")
      (db-init :drop-if-exists true
               :spit-backup 30
               :load-edns true)
      build-handlers-dev))

;;-----------------------------------------------------------
;;  ____  ____   ___  ____  _   _  ____ _____ ___ ___  _   _ 
;; |  _ \|  _ \ / _ \|  _ \| | | |/ ___|_   _|_ _/ _ \| \ | |
;; | |_) | |_) | | | | | | | | | | |     | |  | | | | |  \| |
;; |  __/|  _ <| |_| | |_| | |_| | |___  | |  | | |_| | |\  |
;; |_|   |_| \_\\___/|____/ \___/ \____| |_| |___\___/|_| \_|
;;-----------------------------------------------------------



;; Use this handler for production (same as the inner handler of handler-for-figwheel-dev).
(def prod-handlers api-lists)
(def prod-handlers-for-testing build-handlers-dev)

(defn make-handler-prod
  ;; Sets db (creating tables if necessary) and returns api handler for
  ;; n-app-lists list.
  ([db] (make-handler-prod db false))
  ([db include-resource-public-folder?]
   (do (log/info "Setting up database tables (if non-existant)...")
       (db-init :db-spec db
                :drop-if-exists false
                :dynamic-set-db true
                :spit-backup 30
                :load-edns true)
       (if include-resource-public-folder?
         prod-handlers-for-testing
         prod-handlers))))


