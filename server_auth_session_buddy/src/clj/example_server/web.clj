(ns example-server.web
  (:require [clojure.string :as s]
            [clojure.java.io :as io]
            ;;Web-dev
            [compojure.core :refer [defroutes context routes
                                    GET PUT POST DELETE ANY]]
            [compojure.handler :as ch]
            [compojure.route :as route]
            [ring.middleware.defaults :as rmid]
            [ring.middleware.params :as rmidp]
            [ring.middleware.resource :as rmidr]
            [ring.middleware.multipart-params :as rmidpm]
            [ring.adapter.jetty :as jetty]
            [ring.util.response :refer [file-response resource-response redirect content-type]]
            ;; All apis of external apps are completely public
            [apps.example-server-external-apps :as APPS-APIS]
            ;;DB
            [example-server.db :as db]
            ;;My routes and middleware
            [example-server.routing.middleware.tools
             :refer [log-middleware log-middleware-only-request]]
            [example-server.routing.public-static-sites :as ss]
            [example-server.routing.private-homedocs :as priv-home-docs]
            ;;Environment variables
            [example-server.env-vars :refer [establish-port my-env]]
            ;[environ.core :refer [env]]
            ;;Tools
            [example-server.tools :refer [pformat]]
            ;;Log
            [clojure.tools.logging :as log])
  ;;(:gen-class)
  )

;; Note: For dev, default vals of atoms are:
(def STATIC-HTML-FOLDER-PUBLIC  (atom "./resources/public"))
(def STATIC-HTML-FOLDER-PRIVATE (atom "./resources/private"))

(defn log-path-atoms []
  (log/info (str "STATIC-HTML-FOLDER-PUBLIC  -- " @STATIC-HTML-FOLDER-PUBLIC))
  (log/info (str "STATIC-HTML-FOLDER-PRIVATE -- " @STATIC-HTML-FOLDER-PRIVATE)))

(defn init-routing-paths [{:keys [static-path-public 
                                  static-path-private]
                           :as paths}]
  "Will reset atoms if the arguments are not nil."
  (do (log/info "Current static html folders:")
      (log-path-atoms)
      (log/info (str "Setting up routing paths:" paths))
      (when static-path-public  (reset! STATIC-HTML-FOLDER-PUBLIC  static-path-public))
      (when static-path-private (reset! STATIC-HTML-FOLDER-PRIVATE static-path-private))
      (log/info (str "New static html folders:"))
      (log-path-atoms)))


(defroutes not-found-handler
  (ANY "*" []
       (route/not-found (slurp (io/resource "404.html")))))

(defn build-handlers-redirects []
  (routes
   ;; Redirect "/" to top index file and "/pages" to "/pages/".
   (GET "/" []
       (let [redUrl (str (:serverUrl my-env) "/pages/index.html")]
         (log/debug (str "Redirecting to " redUrl))
         (-> (redirect redUrl)
             (content-type "text/html"))))
   (GET "/pages" [];;TODO: Working?
       (let [redUrl (str (:serverUrl my-env) "/pages/index.html")]
         (log/debug (str "Redirecting to " redUrl))
         (-> (redirect redUrl)
             (content-type "text/html"))))))


(defn build-handlers-api []
  (routes
   ;;BEGIN_EXTERNAL_APPS Apps (non-static ones)
   (APPS-APIS/build-handlers-apis)
   (APPS-APIS/build-handlers-apis-secured)
   ;;END_EXTERNAL_APPS
   ))

(defn make-app []
  ;; Note: The body of a ring request can only be consumed once. Which means
  ;; that routes with middleware that consume the body will need to be first.
  ;; If the same middleware is applied to the same route twice after the
  ;; first one (because it was added in another module, for example), it will
  ;; not override the map. But unfortunately, middlewares in other routes will
  ;; consume the body and find nothing. Check:
  ;; https://github.com/ring-clojure/ring/issues/31
  ;; It means that middleware (that consumes body) like
  ;;      i) wrap-params
  ;;      ii) wrap-multipart-params
  ;; have to wrap the following handlers:
  ;;      - priv-home-docs/get-handler (i)
  ;;      - ls/webpages-list (ii)
  ;; Note: These middlewares that all consume the body have to (possibly?) be
  ;; wrapping the routes (containing the above mentioned handlers) in a
  ;; certain order. For that check:
  ;; https://github.com/ring-clojure/ring-defaults/blob/master/src/ring/middleware/defaults.clj
  ;; 
  ;;Join all handlers together
  (-> (routes (build-handlers-redirects)
              (-> ;;All static files.
                  ss/static-files
                  ;; Log request and response (request is already a map changed by
                  ;; ring-defaults)
                  (log-middleware (str "RECEIVED STATIC FILES ROUTES "
                                       "(after ring's site-defaults):")
                                  "RESPONSE STATIC FILES ROUTES:")
                  ;; ring-defaults/site-defaults adds middleware related to parameters,
                  ;; cookies, sessions, static resources, file uploads, and a bunch of
                  ;; browser-specific security headers.
                  ;; A few middlewares that site-defaults add are:
                  ;;     wrap-session
                  ;;     wrap-params
                  ;; Also: Avoid checking post request for forgeries.
                  ;; Check: https://stackoverflow.com/questions/33132131/unable-to-complete-post-request-in-clojure
                  (rmid/wrap-defaults (-> rmid/site-defaults
                                          (assoc-in [:security :anti-forgery] false)
                                          (assoc-in [:static :files] [@STATIC-HTML-FOLDER-PUBLIC]))))
              ;; Private files
              (priv-home-docs/get-all-handlers-in-one    @STATIC-HTML-FOLDER-PRIVATE)
              (-> (build-handlers-api)
                  (log-middleware "RECEIVED API ROUTES (after ring's api-defaults)"
                                  "RESPONSE API ROUTES")
                  (rmid/wrap-defaults rmid/api-defaults))
              ;; 404 page:
              not-found-handler)
      rmidpm/wrap-multipart-params
      rmidp/wrap-params
      ;;Development: Print stacktrace in browser
      ;;(wrap-stacktrace)
      ;; Log incoming ring-map before being changed by ring-middlewares
      (log-middleware-only-request "ORIGINAL REQUEST: ")))

#_(commnet (def my-app (make-app))
           (my-app {:request-method :get :uri "/homedocs/index.html"})
           (my-app {:request-method :post :uri "/login-buddy"
                    :form-params {"username" "anne" "password" "ardilla"}})
           (my-app {:request-method :post :uri "/login-buddy"
                    :form-params {"username" "nikki" "password" "secret"}})
           (my-app {:request-method :post :uri "/login-buddy?next=/homedocs"
                    :form-params {"username" "nikki" "password" "secret"}})
           (my-app {:request-method :get :uri "/logout"}))


(defn start-server [port {:keys [db-init]
                          :or {db-init true
                               static-path-public  nil
                               static-path-private nil}
                          :as options}]
  "Starts server"
  (do (when db-init (db/init))
      (init-routing-paths options)
      (log/info (str "Starting server at port " port))
      (log/info "Setting up database tables (if non-existant)...")
      (jetty/run-jetty (make-app) ;;Before: #app 
                       {:port port :join? false})))

#_(comment (def server (start-server 3000 {}))
           (doto server .stop))

(defn -main [& [port]]
  ;;Dev: port 5000
  (let [port (establish-port port) ;(Integer. (or port (env :port) 5000))
        ]
    (do
      ;; To init logging, namespace [n-tools-dev.init-log4j2] was loaded (see above).
      (log/info "My environment vars are: "
                 (pformat my-env))
      (log/info (str "Starting server at port " port))
      (log/info "Setting up database tables (if non-existant)...")
      (db/init)
      (jetty/run-jetty (make-app) ;;Before: #app 
                       {:port port :join? false}))))

