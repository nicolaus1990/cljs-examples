(ns n-cljs-examples.sortable-list
  (:require [reagent.core :as r]
            [oops.core :refer [oget oset! ocall oapply ocall! oapply!
                               oget+ oset!+ ocall+ oapply+ ocall!+ oapply!+]]
            ;; Note: some cljsjs libs dont have global-exports support (ie:
            ;; :as and :refer do not work with any library). Check:
            ;; https://github.com/cljsjs/packages/tree/master/react-sortable-hoc
            ;;[cljsjs.react-sortable-hoc]
            [react-sortable-hoc :as sort]))

;; Taken from https://github.com/reagent-project/reagent/blob/master/examples/react-sortable-hoc/src/example/core.cljs
;; who in turn adapted it from https://github.com/clauderic/react-sortable-hoc/blob/master/examples/drag-handle.js#L10


(def DragHandle
  (sort/SortableHandle.
    ;; Alternative to r/reactify-component, which doens't convert props and hiccup,
    ;; is to just provide fn as component and use as-element or create-element
    ;; to return React elements from the component.
   (fn []
     (r/as-element [:span "<::::>"]))))

(def SortableItem
  (sort/SortableElement.
   (r/reactify-component
    (fn [{:keys [value]}]
      [:li
       [:> DragHandle]
       value]))))

;; Alternative without reactify-component
;; props is JS object here
#_(def SortableItem
    (sort/SortableElement.
     (fn [props]
       (r/as-element
        [:li
         [:> DragHandle]
         (.-value props)]))))

(def SortableList
  (sort/SortableContainer.
   (r/reactify-component
    (fn [{:keys [items]}]
      [:ul
       (for [[value index] (map vector items (range))]
           ;; No :> or adapt-react-class here because that would convert value to JS
         (r/create-element
          SortableItem
          #js {:key (str "item-" index)
               :index index
               :value value}))]))))

;; Or using new :r> shortcut, which doesn't do props conversion
#_(def SortableList
    (sort/SortableContainer.
     (r/reactify-component
      (fn [{:keys [items]}]
        [:ul
         (for [[value index] (map vector items (range))]
           [:r> SortableItem
            #js {:key (str "item-" index)
                 :index index
                 :value value}])]))))

(defn vector-move [coll prev-index new-index]
  (let [items (into (subvec coll 0 prev-index)
                    (subvec coll (inc prev-index)))]
    (-> (subvec items 0 new-index)
        (conj (get coll prev-index))
        (into (subvec items new-index)))))

(comment
  (= [0 2 3 4 1 5] (vector-move [0 1 2 3 4 5] 1 4)))

#_(defn sortable-component []
  (let [items (r/atom (vec (map (fn [i] (str "Item " i)) (range 6))))]
    (fn []
      (r/create-element
       SortableList
       #js {:items @items
            :onSortEnd (fn [event]
                         (swap! items vector-move (.-oldIndex event) (.-newIndex event)))
            :useDragHandle true}))))

(defn create-js-obj [items]
  ;; Need to access variables of js objs with oops library to prevent 
  ;; advanced compilation of changing these names.
  #js {:items @items
       :useDragHandle true
       :onSortEnd (fn [event]
                    (swap! items vector-move (oget event "oldIndex") (oget event "newIndex")))})

(defn sortable-component []
  (let [items (r/atom (vec (map (fn [i] (str "Item " i)) (range 6))))]
    (fn []
      (r/create-element
       SortableList
       (create-js-obj items)))))