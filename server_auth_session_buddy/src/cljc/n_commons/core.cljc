(ns n-commons.core
  (:require [clojure.string :as str]
            #?(:clj [clojure.tools.logging :as log])
            #?(:clj [clojure.pprint :as pprint])))

(defn str->int [s]
  #?(:clj  (java.lang.Integer/parseInt s)
     :cljs (js/parseInt s)))

(defn str-contains #?(:clj [^String some-str elem]
                      :cljs [some-str elem])
  (.contains some-str elem))


;;--------------------------------------------------------
;; Log tools
;;
;; Logging levels:
;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
;;
;;--------------------------------------------------------

#?(:clj (defn- log-inner ([type msg] (log-inner type msg nil))
          ([type msg error]
           (case type
             :alert (log-inner :error msg error)
             :debug (log/debug msg)
             :info (log/info msg)
             :warn (log/warn msg)
             ;; For error, check first if it is not nil to be passed as message.
             :error (if error (log/error error msg) (log/error msg))
             :fatal (log/fatal msg)
             ;;Default: ;;(println (apply str txts))
             (log/error (str "log-inner wrong argument! Message was:" msg)))))
   :cljs (defn- log-inner [type msg]
           (if (= :alert type)
             (js/alert msg)
             ;;Ignore type. Just log
             (.log js/console msg))))

(defn debug [msg] (log-inner :debug msg))

(defn- strs-to-str [txts] (apply str txts))

(defn debug-strs [& txts] (debug (strs-to-str txts)))
