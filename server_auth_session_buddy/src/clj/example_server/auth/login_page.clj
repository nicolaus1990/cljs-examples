(ns example-server.auth.login-page
  (:require ;;HTML templating
            [hiccup.core :as hi]
            [hiccup.page :as hip]
            [hiccup.element :as hie]
            ;;Authorization and authentication
            #_[buddy.auth :refer [authenticated? throw-unauthorized]]
            #_[buddy.auth.backends.session :refer [session-backend]]
            #_[buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            ;;Debugging
            [example-server.routing.middleware.tools :refer [log-middleware]]
            [clojure.tools.logging :as log])
  ;(:gen-class)
  )


(def js-client-script
  (str
   "
// Taken from: https://github.com/emn178/js-sha256
//sha256('Message to hash');
//var hash = sha256.create();
//hash.update('Message to hash');
//hash.hex();
function makeHash(message, salt) {
  sha256('Message to hash');
  var hash = sha256.create();
  var u = document.getElementById('user').innerHTML;
  //var p = document.getElementById('password').innerHTML;
  hash.update(u);
  document.getElementById('output').innerHTML = hash.hex();
  return hash.hex();
}
"))

   

;;;;;;;;;;;;;;; Login & Logout pages

(def login-page
  (hi/html ;;hip/html5
   (hip/xhtml-tag
    "en"
    [:head
     [:title "Login"]
     #_[:style {:type "text/css"}
      "body{background:#222;color:#ddd;margin:1em}"
      "h1{border:1px solid;padding:1em}"
      "td{padding:0.3em;border:1px solid #666}"]
     (hip/include-css ;;"https://www.jsdelivr.com/package/npm/bulma"
      "https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css"
      )
     (hip/include-js
      "https://cdnjs.cloudflare.com/ajax/libs/js-sha256/0.9.0/sha256.js")
     (hie/javascript-tag js-client-script)]
    [:body
     [:section {:class "section"}
      [:div {:class "container"}
       [:h1 {:class "title"} "Hi there!"]
       [:p {:class "subtitle"} "(Auth)" ]
       #_[:hr]
       [:div 
        [:form {:method "post"}
         [:div {:class "block"} [:input.input {:type "text" :placeholder "User: " :name "username"}]]
         [:div {:class "block"} [:input.input {:type "password" :placeholder "Password: " :name "password"}]]
         [:div {:class "block"} [:input.button.is-info {:type "submit" :value "Submit"}]]]]
       #_[:div#user {:onclick "makeHash(1,2)"} "someval"]
       #_[:div#output ]]]
     [:footer.footer {:class "footer"}
      [:div.content.has-text-centered {:class "content has-text-centered"}
       [:p "By NM"]
       [:p "Powered by " [:strong "Clojure"] ", " [:strong "Hiccup"] " and "[:strong "Bulma"]]]]])))

(def logout-page
  (hi/html
   [:div
    [:h1 "Come back again!"]
    [:p "You just logged out."]]))

(def error-403
  (hi/html
   [:div
    [:h1 "Sorry, pal. This is not allowed."]
    [:p "... please, stop!"]]))
