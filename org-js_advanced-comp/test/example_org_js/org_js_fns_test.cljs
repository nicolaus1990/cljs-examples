(ns example-org-js.core-test
    (:require
     [cljs.test :refer-macros [deftest is testing]]
     [example-org-js.org-js-fns :as oj]))

#_(deftest multiply-test
  (is (= (* 1 2) (multiply 1 2))))

(deftest example-org-doc-test
  (is (= 2 (+ 1 1))))
